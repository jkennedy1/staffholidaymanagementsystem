package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.Admin;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.DepartmentManager;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.LineManager;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffMember;
import org.apache.commons.dbcp.BasicDataSource;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.samePropertyValuesAs;

public class StaffDaoImplTest {

    private StaffDaoImpl staffDao;
    private JdbcTemplate jdbcTemplate;



    @BeforeEach
    public void before() {
        final BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://127.0.0.1:3306/test_schema?allowMultiQueries=true");
        dataSource.setUsername("root");

        jdbcTemplate = new JdbcTemplate(dataSource);
        staffDao = new StaffDaoImpl(new JdbcTemplate(dataSource));

        String createStaffMembers = " INSERT INTO staff_member(staff_member_role, staff_password, forename, surname, department, line_manager_staff_id, e_mail,  leave_entitlement, leave_remaining, enabled)\n" +
                "VALUES     ('ROLE_DEPARTMENT_MANAGER', '$2a$10$AJ2rm1S8L9jzKoC3Uk4V5upOaP3jvBpn8tsSZ/41zU8jYqJlFBqu.', 'John', 'Kennedy', 'CMP', NULL, 'Kenndey27@hotmail.com', 32.00, 12.0, true),\n" +
                "           ('ROLE_LINE_MANAGER', '$2a$10$GNdbceswM68nfGTmAv7Ji.Et1Nmb.ie/ordBqxx6bw/kMwOH8P.bq', 'Peter', 'Griffin', 'CMP', 1, 'Griffin27@hotmail.com', 32.03, 11.01, true),\n" +
                "           ('ROLE_STAFF', '$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG', 'Josh', 'Nobody', 'CMP', 2, 'Nobody27@hotmail.com', 25.00, 14.00, true),\n" +
                "           ('ROLE_ADMIN', '$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y', 'Mark', 'Someone', 'CMP', 2, 'Someone27@hotmail.com', 20.00, 5.00, true);";

        jdbcTemplate.execute(createStaffMembers);


    }

    @AfterEach
    public void after() {
        String resetTable = "SET foreign_key_checks = 0; TRUNCATE TABLE staff_member; SET foreign_key_checks = 1;";

        jdbcTemplate.execute(resetTable);

    }


    //All staff unchanged from database, copy object code from here
    @Test
    public void testGetAllStaffByDepartment(){
        StaffMember departmentManager = new DepartmentManager(1, "$2a$10$AJ2rm1S8L9jzKoC3Uk4V5upOaP3jvBpn8tsSZ/41zU8jYqJlFBqu.", "John", "Kennedy", "CMP", 0, "Kenndey27@hotmail.com", 32.00F,12.00F, true);
        departmentManager.setStaffMemberRole(StaffType.ROLE_DEPARTMENT_MANAGER);

        StaffMember lineManager = new LineManager(2,"$2a$10$GNdbceswM68nfGTmAv7Ji.Et1Nmb.ie/ordBqxx6bw/kMwOH8P.bq", "Peter", "Griffin", "CMP", 1, "Griffin27@hotmail.com", 32.03F, 11.01F, true);
        lineManager.setStaffMemberRole(StaffType.ROLE_LINE_MANAGER);

       StaffMember staffMember = new StaffMember(3, "$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG", "Josh", "Nobody", "CMP", 2, "Nobody27@hotmail.com", 25.00F, 14.00F, true);

       StaffMember admin = new Admin(4, "$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y", "Mark", "Someone", "CMP", 2, "Someone27@hotmail.com", 20.00F, 5.00F, true);
        admin.setStaffMemberRole(StaffType.ROLE_ADMIN);


        assertThat(staffDao.getAllStaffByDepartment("CMP"), Matchers.hasItems(departmentManager, lineManager, staffMember, admin));
    }

    @Test
    public void testGetAllStaffByLineManager(){

        StaffMember staffMember = new StaffMember(3, "$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG", "Josh", "Nobody", "CMP", 2, "Nobody27@hotmail.com", 25.00F, 14.00F, true);

        StaffMember admin = new Admin(4, "$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y", "Mark", "Someone", "CMP", 2, "Someone27@hotmail.com", 20.00F, 5.00F, true);
        admin.setStaffMemberRole(StaffType.ROLE_ADMIN);


       assertThat(staffDao.getAllStaffByLineManager(2), Matchers.hasItems(staffMember, admin));
    }


    @Test
    public void testGetStaffById() {

        StaffMember departmentManager = new DepartmentManager(1, "$2a$10$AJ2rm1S8L9jzKoC3Uk4V5upOaP3jvBpn8tsSZ/41zU8jYqJlFBqu.", "John", "Kennedy", "CMP", 0, "Kenndey27@hotmail.com", 32.00F,12.00F, true);
        departmentManager.setStaffMemberRole(StaffType.ROLE_DEPARTMENT_MANAGER);

        assertThat(staffDao.getStaffById(1), samePropertyValuesAs(departmentManager));
    }

    @Test
    public void testAddStaff(){

        StaffMember staffMember = new StaffMember(5, "$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG", "Test", "Test123", "CMP", 2, "TestLOL@hotmail.com", 35.00F, 20.00F, true);

        assertThat(staffDao.addStaff(staffMember), Matchers.is(1));
        assertThat(staffDao.getStaffById(5), Matchers.is(staffMember));
    }

    @Test
    public void testUpdateStaff(){
        StaffType staffType = StaffType.ROLE_ADMIN;
        String department = "CMP";
        float leaveEntitlement = 15F;
        float leaveRemaining = 10F;
        int staffId = 3;

        StaffMember staffMember = new Admin(3, "$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG", "Josh", "Nobody", "CMP", 2, "Nobody27@hotmail.com", 15.00F, 10.00F, true);
        staffMember.setStaffMemberRole(StaffType.ROLE_ADMIN);

        assertThat(staffDao.updateStaff(staffType, department, leaveEntitlement, leaveRemaining, staffId), Matchers.is(1));
        assertThat(staffDao.getStaffById(staffId), Matchers.is(staffMember));
    }


    @Test
    public void testToggleEnabledStaffById(){

        assertThat(staffDao.toggleEnabledStaffById(3), Matchers.is(1));
        assertThat(staffDao.getStaffById(3).isEnabled(), Matchers.is(false));

    }

    @Test
    public void testGetAllStaff(){

        StaffMember departmentManager = new DepartmentManager(1, "$2a$10$AJ2rm1S8L9jzKoC3Uk4V5upOaP3jvBpn8tsSZ/41zU8jYqJlFBqu.", "John", "Kennedy", "CMP", 0, "Kenndey27@hotmail.com", 32.00F,12.00F, true);
        departmentManager.setStaffMemberRole(StaffType.ROLE_DEPARTMENT_MANAGER);

        StaffMember lineManager = new LineManager(2,"$2a$10$GNdbceswM68nfGTmAv7Ji.Et1Nmb.ie/ordBqxx6bw/kMwOH8P.bq", "Peter", "Griffin", "CMP", 1, "Griffin27@hotmail.com", 32.03F, 11.01F, true);
        lineManager.setStaffMemberRole(StaffType.ROLE_LINE_MANAGER);

        StaffMember staffMember = new StaffMember(3, "$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG", "Josh", "Nobody", "CMP", 2, "Nobody27@hotmail.com", 25.00F, 14.00F, true);

        StaffMember admin = new Admin(4, "$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y", "Mark", "Someone", "CMP", 2, "Someone27@hotmail.com", 20.00F, 5.00F, true);
        admin.setStaffMemberRole(StaffType.ROLE_ADMIN);

        assertThat(staffDao.getAllStaff(4), Matchers.hasItems(departmentManager, lineManager, staffMember));
        assertThat(staffDao.getAllStaff(4), Matchers.not(hasItems(admin)));
    }

    @Test
    public void testGetAllLineManagers(){

        StaffMember lineManager = new LineManager(2,"$2a$10$GNdbceswM68nfGTmAv7Ji.Et1Nmb.ie/ordBqxx6bw/kMwOH8P.bq", "Peter", "Griffin", "CMP", 1, "Griffin27@hotmail.com", 32.03F, 11.01F, true);
        lineManager.setStaffMemberRole(StaffType.ROLE_LINE_MANAGER);

        StaffMember lineManager2 = new LineManager(5,"$2a$10$GNdbceswM68nfGTmAv7Ji.Et1Nmb.ie/ordBqxx6bw/kMwOH8P.bq", "Jean", "Wilde", "CMP", 1, "JWild@hotmail.com", 40.03F, 25.01F, true);
        lineManager2.setStaffMemberRole(StaffType.ROLE_LINE_MANAGER);


        assertThat(staffDao.addStaff(lineManager2), Matchers.is(1));
        assertThat(staffDao.getStaffById(5), Matchers.samePropertyValuesAs(lineManager2));
        assertThat(staffDao.getAllLineManagers(), Matchers.hasItems(lineManager, lineManager2));
    }

    @Test
    public void testRefundLeave(){

       assertThat(staffDao.getStaffById(3).getLeaveRemaining(), Matchers.is(14.00F));
        assertThat(staffDao.refundLeave(2.30F, 3), Matchers.is(1));
       assertThat(staffDao.getStaffById(3).getLeaveRemaining(), Matchers.is(16.30F));
    }

    @Test
    public void testDeductLeave(){

       assertThat(staffDao.getStaffById(3).getLeaveRemaining(), Matchers.is(14.00F));
        assertThat(staffDao.deductLeave(2.30F, 3), Matchers.is(1));
        assertThat(staffDao.getStaffById(3).getLeaveRemaining(), Matchers.is(11.70F));
    }




}
