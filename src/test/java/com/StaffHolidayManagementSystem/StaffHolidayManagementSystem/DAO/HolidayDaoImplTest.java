package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.AbsenceType;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.HolidayStatus;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffHoliday;
import org.apache.commons.dbcp.BasicDataSource;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;


public class HolidayDaoImplTest {


    private HolidayDaoImpl holidayDao;
    private JdbcTemplate jdbcTemplate;


    @BeforeEach
    public void before() {

        final BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://127.0.0.1:3306/test_schema?allowMultiQueries=true");
        dataSource.setUsername("root");


        jdbcTemplate = new JdbcTemplate(dataSource);
        holidayDao = new HolidayDaoImpl(new JdbcTemplate(dataSource));


        String createStaffMembers = " INSERT INTO staff_member(staff_member_role, staff_password, forename, surname, department, line_manager_staff_id, e_mail,  leave_entitlement, leave_remaining, enabled)\n" +
                "VALUES     ('ROLE_DEPARTMENT_MANAGER', '$2a$10$AJ2rm1S8L9jzKoC3Uk4V5upOaP3jvBpn8tsSZ/41zU8jYqJlFBqu.', 'John', 'Kennedy', 'CMP', NULL, 'Kenndey27@hotmail.com', 32.00, 12.0, true),\n" +
                "           ('ROLE_LINE_MANAGER', '$2a$10$GNdbceswM68nfGTmAv7Ji.Et1Nmb.ie/ordBqxx6bw/kMwOH8P.bq', 'Peter', 'Griffin', 'CMP', 1, 'Griffin27@hotmail.com', 32.03, 11.01, true),\n" +
                "           ('ROLE_STAFF', '$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG', 'Josh', 'Nobody', 'CMP', 2, 'Nobody27@hotmail.com', 25.00, 14.00, true),\n" +
                "           ('ROLE_ADMIN', '$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y', 'Mark', 'Someone', 'CMP', 2, 'Someone27@hotmail.com', 20.00, 5.00, true);";

        String createHolidays = "INSERT INTO holiday(staff_member_id, start_date, end_date, holiday_reason, absence_type, manager_comment, holiday_status, alt_holiday_id)\n" +
                "VALUES  (3, '2019-02-27 12:00:00' , '2019-02-28 09:00:00', 'N/A' ,'UNPAIDLEAVE', 'No Comments', 'ARCHIVED', 0),\n" +
                "(3, '2020-03-28 12:00:00' , '2020-03-30 09:00:00', 'N/A' ,'PAIDLEAVE', 'No Comments', 'PENDING', 0),\n" +
                "(3, '2019-04-28 09:00:00' , '2019-04-30 12:00:00', 'Going to Wales' ,'VACATIONLEAVE', 'No Comments', 'ACCEPTED', 0),\n" +
                "(3, '2019-05-28 12:00:00' , '2019-05-30 09:00:00', 'Going to Manchester' ,'VACATIONLEAVE', 'No Comments', 'ACCEPTED', 0),\n" +
                "(3, '2019-06-28 09:00:00' , '2019-06-30 09:00:00', 'Going to Liverpool' ,'VACATIONLEAVE','No Comments', 'PENDING', 0),\n" +
                "(3, '2019-07-28 09:00:00' , '2019-07-30 09:00:00', 'Going to Margate' ,'VACATIONLEAVE','No Comments', 'PENDING', 0),\n" +
                "(3, '2019-08-28 09:00:00' , '2019-08-30 12:00:00', 'Going to York' ,'VACATIONLEAVE', 'Too many leave requests that week', 'DECLINED',8),\n" +
                "(3, '2019-09-27 09:00:00' , '2019-09-30 09:00:00', 'Going to York' ,'VACATIONLEAVE', 'Alternative offer', 'ALTERNATIVEOFFER', 0),\n" +
                "(3, '2019-10-28 09:00:00' , '2019-10-30 09:00:00', 'N/A' ,'MATERNITYLEAVE', 'No Comments', 'PENDING',0),\n" +
                "(3, '2019-08-28 09:00:00' , '2019-08-30 12:00:00', 'Going to York' ,'VACATIONLEAVE', 'Testing the waters', 'PENDING',0);";

        jdbcTemplate.execute(createStaffMembers);
        jdbcTemplate.execute(createHolidays);
    }


    @AfterEach
    public void after() {
        String resetTable = "SET foreign_key_checks = 0; TRUNCATE TABLE holiday; TRUNCATE TABLE staff_member; SET foreign_key_checks = 1;";

        jdbcTemplate.execute(resetTable);
    }


    @Test
    public void testGetHolidayById() {
        StaffHoliday staffHoliday = new StaffHoliday();
        staffHoliday.setHolidayId(1);

        assertThat(holidayDao.getHolidayById(1, 3), Matchers.is(staffHoliday));

    }

    @Test
    public void testGetHolidaysByStaffId() {

        StaffHoliday staffHoliday1 = new StaffHoliday();
        StaffHoliday staffHoliday2 = new StaffHoliday();
        StaffHoliday staffHoliday3 = new StaffHoliday();
        StaffHoliday staffHoliday4 = new StaffHoliday();
        StaffHoliday staffHoliday5 = new StaffHoliday();
        StaffHoliday staffHoliday6 = new StaffHoliday();
        StaffHoliday staffHoliday7 = new StaffHoliday();
        StaffHoliday staffHoliday8 = new StaffHoliday();
        StaffHoliday staffHoliday9 = new StaffHoliday();
        StaffHoliday staffHoliday10 = new StaffHoliday();

        staffHoliday1.setHolidayId(1);
        staffHoliday2.setHolidayId(2);
        staffHoliday3.setHolidayId(3);
        staffHoliday4.setHolidayId(4);
        staffHoliday5.setHolidayId(5);
        staffHoliday6.setHolidayId(6);
        staffHoliday7.setHolidayId(7);
        staffHoliday8.setHolidayId(8);
        staffHoliday9.setHolidayId(9);
        staffHoliday10.setHolidayId(10);


        assertThat(holidayDao.getHolidaysByStaffId(3), Matchers.hasItems(staffHoliday1, staffHoliday2, staffHoliday3, staffHoliday4, staffHoliday5,
                staffHoliday6, staffHoliday7, staffHoliday8, staffHoliday9, staffHoliday10));
    }

    @Test
    public void testGetAcceptedAndPendingHolidaysByYear() {

        StaffHoliday staffHoliday3 = new StaffHoliday();
        StaffHoliday staffHoliday4 = new StaffHoliday();
        StaffHoliday staffHoliday5 = new StaffHoliday();
        StaffHoliday staffHoliday6 = new StaffHoliday();
        StaffHoliday staffHoliday9 = new StaffHoliday();

        staffHoliday3.setHolidayId(3);
        staffHoliday4.setHolidayId(4);
        staffHoliday5.setHolidayId(5);
        staffHoliday6.setHolidayId(6);
        staffHoliday9.setHolidayId(9);

        assertThat(holidayDao.getAcceptedAndPendingHolidaysByYear(3, 2019), Matchers.hasItems(staffHoliday3, staffHoliday4, staffHoliday5, staffHoliday6, staffHoliday9));

    }

    @Test
    public void testAddHoliday() {

        LocalDateTime startDate = LocalDateTime.of(2019, 4, 27, 9, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 4, 28, 9, 0);

        StaffHoliday staffHoliday = new StaffHoliday(11, startDate, endDate, "N/A", AbsenceType.ADOPTIONLEAVE, "N/A", HolidayStatus.PENDING, 0);

        assertThat(holidayDao.addHoliday(staffHoliday, 3), Matchers.is(1));
        assertThat(holidayDao.getHolidayById(11, 3), Matchers.samePropertyValuesAs(staffHoliday));
    }

    @Test
    public void testDeclineLeaveById() {

        assertThat(holidayDao.getHolidayById(5, 3).getHolidayStatus(), Matchers.is(HolidayStatus.PENDING));
        assertThat(holidayDao.declineLeaveById(5, 3, HolidayStatus.DECLINED), Matchers.is(1));
        assertThat(holidayDao.getHolidayById(5, 3).getHolidayStatus(), Matchers.is(HolidayStatus.DECLINED));
    }

    @Test
    public void testGetLastFiveNoneArchivedHolidaysByStaffId() {

        StaffHoliday staffHoliday5 = new StaffHoliday();
        StaffHoliday staffHoliday6 = new StaffHoliday();
        StaffHoliday staffHoliday7 = new StaffHoliday();
        StaffHoliday staffHoliday9 = new StaffHoliday();
        StaffHoliday staffHoliday10 = new StaffHoliday();


        staffHoliday5.setHolidayId(5);
        staffHoliday6.setHolidayId(6);
        staffHoliday7.setHolidayId(7);
        staffHoliday9.setHolidayId(9);
        staffHoliday10.setHolidayId(10);

        assertThat(holidayDao.getLastFiveNoneArchivedHolidaysByStaffId(3), Matchers.hasItems(staffHoliday5, staffHoliday6, staffHoliday7, staffHoliday9, staffHoliday10));
    }

    @Test
    public void testGetLeaveEventsForCalendar() {

        LocalDateTime startDate = LocalDateTime.of(2019, 1, 1, 9, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 5, 29, 9, 0);

        StaffHoliday staffHoliday3 = new StaffHoliday();
        StaffHoliday staffHoliday4 = new StaffHoliday();

        staffHoliday3.setHolidayId(3);
        staffHoliday4.setHolidayId(4);

        assertThat(holidayDao.getLeaveEventsForCalendar(3, startDate, endDate), Matchers.hasItems(staffHoliday3, staffHoliday4));

    }

    @Test
    public void testUpdateLeaveStatus() {

        assertThat(holidayDao.getHolidayById(5, 3).getHolidayStatus(), Matchers.is(HolidayStatus.PENDING));
        assertThat(holidayDao.updateLeaveStatus(5, 3, HolidayStatus.DECLINED_ALT_OFFER_ARCHIVED), Matchers.is(1));
        assertThat(holidayDao.getHolidayById(5, 3).getHolidayStatus(), Matchers.is(HolidayStatus.DECLINED_ALT_OFFER_ARCHIVED));
    }

    @Test
    public void testGetHolidayIdByStartAndEndDate() {

        LocalDateTime startDate = LocalDateTime.of(2020, 3, 28, 12, 0);
        LocalDateTime endDate = LocalDateTime.of(2020, 3, 30, 9, 0);

        assertThat(holidayDao.getHolidayIdByStartAndEndDate(3, startDate, endDate), Matchers.is(2));

    }

    @Test
    public void testUpdateDeclinedLeaveWithAltLeave() {

        StaffHoliday staffHoliday = new StaffHoliday();
        staffHoliday.setHolidayId(10);

        assertThat(holidayDao.getHolidayById(10, 3).getHolidayStatus(), Matchers.is(HolidayStatus.PENDING));
        assertThat(holidayDao.updateDeclinedLeaveWithAltLeave(3, "N/A", 10, 8, HolidayStatus.DECLINED), Matchers.is(1));
        assertThat(holidayDao.getHolidayById(10, 3).getHolidayStatus(), Matchers.is(HolidayStatus.DECLINED));
        assertThat(holidayDao.getHolidayById(10, 3).getAltHolidayId(), Matchers.is(8));
    }

    @Test
    public void testSimpleManagerDeclineLeave() {

        assertThat(holidayDao.getHolidayById(10, 3).getHolidayStatus(), Matchers.is(HolidayStatus.PENDING));
        assertThat(holidayDao.simpleManagerDeclineLeave(3, 10, "Test", HolidayStatus.DECLINED), Matchers.is(1));
        assertThat(holidayDao.getHolidayById(10, 3).getHolidayStatus(), Matchers.is(HolidayStatus.DECLINED));

    }


}
