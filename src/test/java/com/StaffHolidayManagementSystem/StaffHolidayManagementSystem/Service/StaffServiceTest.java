package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Service;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO.HolidayDao;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO.StaffDao;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO.StaffType;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.*;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper.PasswordEncrypter;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper.ReturnMessage;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;


public class StaffServiceTest {


    @Mock
    StaffDao staffDao;
    @Mock
    HolidayDao holidayDao;
    @Mock
    PasswordEncrypter passwordEncrypter;




    private StaffService staffService;

    @BeforeEach
    public void before() {
        MockitoAnnotations.initMocks(this);
        staffService = new StaffService(staffDao, holidayDao);
        passwordEncrypter = new PasswordEncrypter();


    }


    @Test
    public void testGetStaffMemberById() {
        StaffMember staffMember = new StaffMember();
        StaffMember staffMember2 = new StaffMember();
        List<StaffHoliday> holidayList = new ArrayList<>();
        LocalDateTime startDate = LocalDateTime.of(2019, 4, 26, 9, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 4, 27, 9, 0);
        holidayList.add(new StaffHoliday(0, startDate, endDate, "N/A", AbsenceType.ADOPTIONLEAVE, "No Comments", HolidayStatus.PENDING, 0));
        staffMember2.addStaffHoliday(new StaffHoliday(0, startDate, endDate, "N/A", AbsenceType.ADOPTIONLEAVE, "No Comments", HolidayStatus.PENDING, 0));

        Mockito.when(staffDao.getStaffById(1)).thenReturn(staffMember);
        Mockito.when(holidayDao.getHolidaysByStaffId(1)).thenReturn(holidayList);


      assertThat(staffService.getStaffMemberById(1), samePropertyValuesAs(staffMember2));
    }

    @Test
    public void testGetAllStaffByDepartment() {

        StaffMember departmentManager = new DepartmentManager(1, "$2a$10$AJ2rm1S8L9jzKoC3Uk4V5upOaP3jvBpn8tsSZ/41zU8jYqJlFBqu.", "John", "Kennedy", "CMP", 0, "Kenndey27@hotmail.com", 32.00F, 12.00F, true);
        departmentManager.setStaffMemberRole(StaffType.ROLE_DEPARTMENT_MANAGER);

        StaffMember lineManager = new LineManager(2, "$2a$10$GNdbceswM68nfGTmAv7Ji.Et1Nmb.ie/ordBqxx6bw/kMwOH8P.bq", "Peter", "Griffin", "CMP", 1, "Griffin27@hotmail.com", 32.03F, 11.01F, true);
        lineManager.setStaffMemberRole(StaffType.ROLE_LINE_MANAGER);

        StaffMember staffMember = new StaffMember(3, "$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG", "Josh", "Nobody", "CMP", 2, "Nobody27@hotmail.com", 25.00F, 14.00F, true);

        StaffMember admin = new Admin(4, "$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y", "Mark", "Someone", "CMP", 2, "Someone27@hotmail.com", 20.00F, 5.00F, true);
        admin.setStaffMemberRole(StaffType.ROLE_ADMIN);

        List<StaffMember> staffMemberList = new ArrayList<>() {
            {
                add(staffMember);
                add(admin);
                add(lineManager);
                add(departmentManager);
            }
        };

        Mockito.when(staffDao.getStaffById(1)).thenReturn(departmentManager);
        Mockito.when(staffDao.getAllStaffByDepartment("CMP")).thenReturn(staffMemberList);
        assertThat(staffService.getAllStaffByDepartment(1), Matchers.hasItems(staffMember, admin, lineManager, departmentManager));

    }

    @Test
    public void testGetAllStaffByLineManagerByMonth() {

        StaffMember staffMember = new StaffMember(3, "$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG", "Josh", "Nobody", "CMP", 2, "Nobody27@hotmail.com", 25.00F, 14.00F, true);

        StaffMember admin = new Admin(4, "$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y", "Mark", "Someone", "CMP", 2, "Someone27@hotmail.com", 20.00F, 5.00F, true);
        admin.setStaffMemberRole(StaffType.ROLE_ADMIN);

        LocalDateTime startDate = LocalDateTime.of(2019, 2, 1, 9, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 2, 28, 9, 0);

        List<StaffMember> staffMemberList = new ArrayList<>() {
            {
                add(staffMember);
                add(admin);
            }
        };

        List<StaffHoliday> staffHolidays = new ArrayList<>();

        Mockito.when(staffDao.getAllStaffByLineManager(2)).thenReturn(staffMemberList);
        Mockito.when(holidayDao.getLeaveEventsForCalendar(3, startDate,endDate)).thenReturn(staffHolidays);
        assertThat(staffService.getAllStaffByLineManagerByMonth(2, startDate,endDate), Matchers.hasItems(staffMember, admin));

    }


    @Test
    public void testGetAllStaffByLineManagerByYear(){

        StaffMember staffMember = new StaffMember(3, "$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG", "Josh", "Nobody", "CMP", 2, "Nobody27@hotmail.com", 25.00F, 14.00F, true);

        StaffMember admin = new Admin(4, "$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y", "Mark", "Someone", "CMP", 2, "Someone27@hotmail.com", 20.00F, 5.00F, true);
        admin.setStaffMemberRole(StaffType.ROLE_ADMIN);


        //StaffMembers who have line manager of id = 2
        List<StaffMember> staffMemberList = new ArrayList<>() {
            {
                add(staffMember);
                add(admin);
            }
        };
        //Holidays for 2019 from staffMember
        List<StaffHoliday> staffHolidays = new ArrayList<>();

        Mockito.when(staffDao.getAllStaffByLineManager(2)).thenReturn(staffMemberList);
        Mockito.when(holidayDao.getAcceptedAndPendingHolidaysByYear(3, 2019)).thenReturn(staffHolidays);
        Mockito.when(holidayDao.getAcceptedAndPendingHolidaysByYear(4, 2019)).thenReturn(staffHolidays);
        assertThat(staffService.getAllStaffByLineManagerByYear(2,2019), Matchers.hasItems(staffMember, admin));

    }

    @Test
    public void testGetAllStaffByLineManager(){

        StaffMember staffMember = new StaffMember(3, "$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG", "Josh", "Nobody", "CMP", 2, "Nobody27@hotmail.com", 25.00F, 14.00F, true);

        StaffMember admin = new Admin(4, "$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y", "Mark", "Someone", "CMP", 2, "Someone27@hotmail.com", 20.00F, 5.00F, true);
        admin.setStaffMemberRole(StaffType.ROLE_ADMIN);


        //StaffMembers who have line manager of id = 2
        List<StaffMember> staffMemberList = new ArrayList<>() {
            {
                add(staffMember);
                add(admin);
            }
        };

        List<StaffHoliday> staffHolidays = new ArrayList<>();

        Mockito.when(staffDao.getAllStaffByLineManager(2)).thenReturn(staffMemberList);
        Mockito.when(holidayDao.getAcceptedAndPendingHolidaysByYear(3, 2019)).thenReturn(staffHolidays);
        Mockito.when(holidayDao.getAcceptedAndPendingHolidaysByYear(4, 2019)).thenReturn(staffHolidays);
        assertThat(staffService.getAllStaffByLineManager(2), Matchers.hasItems(staffMember, admin));

    }

    @Test
    public void testGetAllStaffIsAdmin(){


        StaffMember departmentManager = new DepartmentManager(1, "$2a$10$AJ2rm1S8L9jzKoC3Uk4V5upOaP3jvBpn8tsSZ/41zU8jYqJlFBqu.", "John", "Kennedy", "CMP", 0, "Kenndey27@hotmail.com", 32.00F, 12.00F, true);
        departmentManager.setStaffMemberRole(StaffType.ROLE_DEPARTMENT_MANAGER);

        StaffMember lineManager = new LineManager(2, "$2a$10$GNdbceswM68nfGTmAv7Ji.Et1Nmb.ie/ordBqxx6bw/kMwOH8P.bq", "Peter", "Griffin", "CMP", 1, "Griffin27@hotmail.com", 32.03F, 11.01F, true);
        lineManager.setStaffMemberRole(StaffType.ROLE_LINE_MANAGER);

        StaffMember staffMember = new StaffMember(3, "$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG", "Josh", "Nobody", "CMP", 2, "Nobody27@hotmail.com", 25.00F, 14.00F, true);

        StaffMember admin = new Admin(4, "$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y", "Mark", "Someone", "CMP", 2, "Someone27@hotmail.com", 20.00F, 5.00F, true);
        admin.setStaffMemberRole(StaffType.ROLE_ADMIN);


        //StaffMembers who have line manager of id = 2
        List<StaffMember> staffMemberList = new ArrayList<>() {
            {
                add(staffMember);
                add(lineManager);
                add(departmentManager);

            }
        };


        Mockito.when(staffDao.getStaffById(4)).thenReturn(admin);
        Mockito.when(staffDao.getAllStaff(4)).thenReturn(staffMemberList);

       assertThat(staffService.getAllStaff(4), Matchers.hasItems(staffMember, lineManager, departmentManager));
    }


    @Test
    public void testGetAllStaffNotAdmin(){

        StaffMember staffMember = new StaffMember(3, "$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG", "Josh", "Nobody", "CMP", 2, "Nobody27@hotmail.com", 25.00F, 14.00F, true);

        Mockito.when(staffDao.getStaffById(3)).thenReturn(staffMember);

        assertThat(staffService.getAllStaff(3), Matchers.is(new ArrayList()));
    }

    @Test
    public void testEnabledStaffById(){
        StaffMember admin = new Admin(4, "$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y", "Mark", "Someone", "CMP", 2, "Someone27@hotmail.com", 20.00F, 5.00F, true);
        admin.setStaffMemberRole(StaffType.ROLE_ADMIN);


        Mockito.when(staffDao.getStaffById(4)).thenReturn(admin);
        Mockito.when(staffDao.toggleEnabledStaffById(3)).thenReturn(1);


        assertThat(staffService.toggleEnabledStaffById(4,3), Matchers.is(ReturnMessage.SUCCESS.toString()));
    }

    @Test
    public void testUpdateStaffByID(){


        //not yet tested
    }

    @Test
    public void testGetAllLineManagers(){


        StaffMember lineManager = new LineManager(2, "$2a$10$GNdbceswM68nfGTmAv7Ji.Et1Nmb.ie/ordBqxx6bw/kMwOH8P.bq", "Peter", "Griffin", "CMP", 1, "Griffin27@hotmail.com", 32.03F, 11.01F, true);
        lineManager.setStaffMemberRole(StaffType.ROLE_LINE_MANAGER);

        List<StaffMember> staffMemberList = new ArrayList<>();
        staffMemberList.add(lineManager);

        Mockito.when(staffDao.getAllLineManagers()).thenReturn(staffMemberList);

        assertThat(staffService.getAllLineManagers(), Matchers.hasItems(lineManager));
    }

    @Test
    public void testAddStaff(){

        //not yet tested


    }


}
