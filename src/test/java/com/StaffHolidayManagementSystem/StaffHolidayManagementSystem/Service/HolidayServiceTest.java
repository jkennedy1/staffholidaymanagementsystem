package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Service;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO.HolidayDao;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO.StaffDao;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffHoliday;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class HolidayServiceTest {

    @Mock
    HolidayDao holidayDao;
    @Mock
    StaffDao staffDao;

    private HolidayService holidayService;


    @BeforeEach
    public void before() {
        MockitoAnnotations.initMocks(this);
        holidayService = new HolidayService(holidayDao, staffDao);

    }

    @Test
    public void testGetHolidayById() {
        final StaffHoliday holiday = Mockito.mock(StaffHoliday.class);

        Mockito.when(holidayDao.getHolidayById(1, 10)).thenReturn(holiday);
        assertThat(holidayService.getHolidayById(1, 10), Matchers.is(holiday));
    }


    @Test
    void testGetHolidaysByStaffId() {

        //No Test Yet
    }

    @Test
    void testAddHolidayByJsonString() {
        //No Test Yet
    }

    @Test
    void testDeclineLeave() {
        //No Test Yet
    }

    @Test
    void testGetLastFiveNoneArchiveHolidaysByStaffId() {

        StaffHoliday staffHoliday5 = new StaffHoliday();
        StaffHoliday staffHoliday6 = new StaffHoliday();
        StaffHoliday staffHoliday7 = new StaffHoliday();
        StaffHoliday staffHoliday9 = new StaffHoliday();
        StaffHoliday staffHoliday10 = new StaffHoliday();


        staffHoliday5.setHolidayId(5);
        staffHoliday6.setHolidayId(6);
        staffHoliday7.setHolidayId(7);
        staffHoliday9.setHolidayId(9);
        staffHoliday10.setHolidayId(10);

        List<StaffHoliday> staffList = new ArrayList<>(){
            {
                add(staffHoliday5);
                add(staffHoliday6);
                add(staffHoliday7);
                add(staffHoliday9);
                add(staffHoliday10);
            }
        };



        Mockito.when(holidayDao.getLastFiveNoneArchivedHolidaysByStaffId(3)).thenReturn(staffList);
        assertThat(holidayService.getLastFiveNoneArchiveHolidaysByStaffId(3), Matchers.is(staffList));
    }

    @Test
    void testGetLeaveEventsForCalendar() {

        LocalDateTime startDate = LocalDateTime.of(2019, 1, 1, 9, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 5, 29, 9, 0);

        StaffHoliday staffHoliday3 = new StaffHoliday();
        StaffHoliday staffHoliday4 = new StaffHoliday();

        staffHoliday3.setHolidayId(3);
        staffHoliday4.setHolidayId(4);

        List<StaffHoliday> staffList = new ArrayList<>();

        staffList.add(staffHoliday3);
        staffList.add(staffHoliday4);

        Mockito.when(holidayDao.getLeaveEventsForCalendar(3,startDate,endDate)).thenReturn(staffList);

        assertThat(holidayService.getLeaveEventsForCalendar(3, startDate,endDate), Matchers.is(staffList));
    }

    @Test
    void testGetAlternativeOffer() {
        StaffHoliday staffHoliday = new StaffHoliday();
        staffHoliday.setHolidayId(8);

        Mockito.when(holidayService.getHolidayById(8,3)).thenReturn(staffHoliday);

        assertThat(holidayService.getHolidayById(8, 3).getHolidayId(), Matchers.is(8));
    }

    @Test
    void testAcceptAlternativeOffer() {

        //No Test Yet

    }

    @Test
    void testGetStaffLeaveHistoryByStaffId() {

        StaffHoliday staffHoliday1 = new StaffHoliday();
        StaffHoliday staffHoliday2 = new StaffHoliday();
        StaffHoliday staffHoliday3 = new StaffHoliday();
        StaffHoliday staffHoliday4 = new StaffHoliday();
        StaffHoliday staffHoliday5 = new StaffHoliday();
        StaffHoliday staffHoliday6 = new StaffHoliday();
        StaffHoliday staffHoliday7 = new StaffHoliday();
        StaffHoliday staffHoliday8 = new StaffHoliday();
        StaffHoliday staffHoliday9 = new StaffHoliday();
        StaffHoliday staffHoliday10 = new StaffHoliday();

        staffHoliday1.setHolidayId(1);
        staffHoliday2.setHolidayId(2);
        staffHoliday3.setHolidayId(3);
        staffHoliday4.setHolidayId(4);
        staffHoliday5.setHolidayId(5);
        staffHoliday6.setHolidayId(6);
        staffHoliday7.setHolidayId(7);
        staffHoliday8.setHolidayId(8);
        staffHoliday9.setHolidayId(9);
        staffHoliday10.setHolidayId(10);


        List<StaffHoliday> staffList = new ArrayList<>(){
            {
                add(staffHoliday1);
                add(staffHoliday2);
                add(staffHoliday3);
                add(staffHoliday4);
                add(staffHoliday5);
                add(staffHoliday6);
                add(staffHoliday7);
                add(staffHoliday8);
                add(staffHoliday9);
                add(staffHoliday10);

            }
        };
        Mockito.when(holidayDao.getHolidaysByStaffId(3)).thenReturn(staffList);
        staffList.remove(8);

        assertThat(holidayService.getStaffLeaveHistoryByStaffId(3), Matchers.is(staffList));
    }

    @Test
    void testUpdateLeaveStatus() {

        //No Test Yet
    }

    @Test
    void testCreateAltLeave() {

        //No Test Yet
    }

    @Test
    void testSimpleManagerDeclineLeave() {

        //No Test Yet
    }
}
