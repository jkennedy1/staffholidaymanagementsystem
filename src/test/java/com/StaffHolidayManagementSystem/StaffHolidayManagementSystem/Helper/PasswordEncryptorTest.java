package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PasswordEncryptorTest {

    private PasswordEncrypter passwordEncrypter;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @BeforeEach
    public void before(){passwordEncrypter = new PasswordEncrypter(); bCryptPasswordEncoder = new BCryptPasswordEncoder();}


    @Test
    public void testEncryptPasswordNumerically(){

        String unEncryptedPassword = "123456789";
        String encryptedPassword = passwordEncrypter.encryptPassword(unEncryptedPassword);


       assertTrue(bCryptPasswordEncoder.matches(unEncryptedPassword, encryptedPassword));
    }

    @Test
    public void testEncryptPasswordNoNumbers(){

        String unEncryptedPassword = "TestingThisPassword";
        String encryptedPassword = passwordEncrypter.encryptPassword(unEncryptedPassword);
        assertTrue(bCryptPasswordEncoder.matches(unEncryptedPassword, encryptedPassword));
    }



}
