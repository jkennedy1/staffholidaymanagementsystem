package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.AbsenceType;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.HolidayStatus;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffHoliday;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;

public class JsonToStaffHolidayTest {


    private JsonToStaffHoliday jsonToStaffHoliday;

    @BeforeEach
    public void before(){jsonToStaffHoliday = new JsonToStaffHoliday();}


    @Test
    public void testUtcToLondonDateTimeParserEightToNineHour(){

        String dateString = "2019-05-14T08:00:00.000Z";
        LocalDateTime convertedDate = LocalDateTime.of(2019, 5, 14, 9,0);

        assertThat(jsonToStaffHoliday.utcToLondonDateTimeParser(dateString), Matchers.is(convertedDate));

    }

    @Test
    public void testUtcToLondonDateTimeParserEightToElevenToTwelveHour(){

        String dateString = "2019-05-14T11:00:00.000Z";
        LocalDateTime convertedDate = LocalDateTime.of(2019, 5, 14, 12,0);

        assertThat(jsonToStaffHoliday.utcToLondonDateTimeParser(dateString), Matchers.is(convertedDate));

    }

    @Test
    public void testConvertJsonObjToStaffHoliday() throws JSONException {

        String jsonHolidayString = "{\"holidayId\":0,\"startDateTime\":\"2019-05-20T08:00:00.000Z\",\"endDateTime\":\"2019-05-22T08:00:00.000Z\",\"holidayReason\":\"N/A\",\"absenceType\":\"ADOPTIONLEAVE\",\"managerComment\":\"No Comments\",\"holidayStatus\":\"PENDING\",\"altHolidayId\":0}\n";
        JSONObject jsonHolidayObject = new JSONObject(jsonHolidayString);
        LocalDateTime startDate = jsonToStaffHoliday.utcToLondonDateTimeParser(jsonHolidayObject.getString("startDateTime"));
        LocalDateTime endDate = jsonToStaffHoliday.utcToLondonDateTimeParser(jsonHolidayObject.getString("endDateTime"));
        StaffHoliday staffHoliday = new StaffHoliday(0, startDate, endDate, "N/A", AbsenceType.ADOPTIONLEAVE, "No Comments", HolidayStatus.PENDING, 0);


        assertThat(jsonToStaffHoliday.convertJsonObjToStaffHoliday(jsonHolidayObject), samePropertyValuesAs(staffHoliday));
    }


}
