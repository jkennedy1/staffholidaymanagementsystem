package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;


public class LeaveCalculatorTest {

    private LeaveCalculator leaveCalculator;

    @BeforeEach
    public void before() {
        leaveCalculator = new LeaveCalculator();
    }

    @Test
    public void testCalculateFullWorkingDaysBetweenDatesSevenDays() {
        LocalDateTime startDate = LocalDateTime.of(2019, 4, 1, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 4, 7, 0, 0);

        assertThat(leaveCalculator.calculateFullWorkingDaysBetweenDates(startDate, endDate), Matchers.is(5L));

    }

    @Test
    public void testCalculateFullWorkingDaysBetweenDatesThreeDays() {
        LocalDateTime startDate = LocalDateTime.of(2019, 4, 1, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 4, 4, 0, 0);

        assertThat(leaveCalculator.calculateFullWorkingDaysBetweenDates(startDate, endDate), Matchers.is(3L));

    }

    @Test
    public void testCalculateFullWorkingDaysBetweenDatesOneMonth() {
        LocalDateTime startDate = LocalDateTime.of(2019, 4, 1, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 5, 1, 0, 0);

        assertThat(leaveCalculator.calculateFullWorkingDaysBetweenDates(startDate, endDate), Matchers.is(22L));

    }


    @Test
    public void testCalculateFullWorkingDaysBetweenDatesOneYear() {
        LocalDateTime startDate = LocalDateTime.of(2019, 1, 1, 0, 0);
        LocalDateTime endDate = LocalDateTime.of(2020, 1, 1, 0, 0);

        assertThat(leaveCalculator.calculateFullWorkingDaysBetweenDates(startDate, endDate), Matchers.is(261L));

    }


    //Half Day tests values were gathered using https://www.calculatorsoup.com/calculators/math/percentage.php

    @Test
    public void testCalculateBusinessDaysFromHalfDaysOneWorkingDayHalfDayStart() {
        LocalDateTime startDate = LocalDateTime.of(2019, 4, 1, 12, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 4, 2, 9, 0);

        long amountOfDays = 1L;

        //5 hours leave, 3 hours worked on start date
        assertThat(leaveCalculator.calculateBusinessDaysFromHalfDays(amountOfDays, startDate, endDate), Matchers.is(0.63F));

    }

    @Test
    public void testCalculateBusinessDaysFromHalfDaysThreeWorkingDayHalfDayStart() {
        LocalDateTime startDate = LocalDateTime.of(2019, 4, 1, 12, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 4, 4, 9, 0);

        long amountOfDays = 3L;

        //2 days and 5 hours leave, 3 hours worked on start date
       assertThat(leaveCalculator.calculateBusinessDaysFromHalfDays(amountOfDays, startDate, endDate), Matchers.is(2.63F));

    }

    @Test
    public void testCalculateBusinessDaysFromHalfDaysOneWorkingDayHalfDayEnd() {
        LocalDateTime startDate = LocalDateTime.of(2019, 4, 1, 9, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 4, 2, 12, 0);

        long amountOfDays = 1L;

        //1 day leave, 3 hours leave on end date
        assertThat(leaveCalculator.calculateBusinessDaysFromHalfDays(amountOfDays, startDate, endDate), Matchers.is(1.38F));

    }

    @Test
    public void testCalculateBusinessDaysFromHalfDaysHalfDayOneWorkingDayHalfDayEndAndStart() {
        LocalDateTime startDate = LocalDateTime.of(2019, 4, 1, 12, 0);
        LocalDateTime endDate = LocalDateTime.of(2019, 4, 2, 12, 0);

        long amountOfDays = 1L;

        //1 day leave, 5 hours leave on start and 3 hours on end = 8 hours
        assertThat(leaveCalculator.calculateBusinessDaysFromHalfDays(amountOfDays, startDate, endDate), Matchers.is(1F));

    }

}
