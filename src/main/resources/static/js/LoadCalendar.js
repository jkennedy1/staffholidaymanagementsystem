var calendar;
document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');

    calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['dayGrid', 'interaction', 'list', 'bootstrap'],
            defaultView: 'dayGridMonth',
            defaultDate: new Date(),
            selectable: true,
            themeSystem: 'bootstrap',
            header: {
                left: 'prev, today, next',
                center: 'title',
                right: 'dayGridMonth, listYear'
            },
            events:

            //Users Holidays
                {
                    url: '/staff/book_leave/getLeaveEventsForCalendar',
                    textColor: 'white'
                },


            eventDataTransform: function (eventData) {

                var color;

                switch (eventData.absenceType) {

                    case 'ACCEPTED':
                        color = 'Green';
                        break;
                    // case 'PENDING' : color = 'YELLOW'; break;
                    case 'default':
                        break;

                }


                var event = {
                    "id": eventData.holidayId,
                    "title": "Type: " + eventData.absenceType + " Status: " + eventData.holidayStatus,
                    "description": eventData.holidayReason,
                    "start": eventData.startDateTime,
                    "end": eventData.endDateTime
                }

                return event;

            },
            eventTimeFormat: { // like '14:30:00'
                hour: '2-digit'
            },

            buttonText: {
                list: 'Year List',
                dayGridMonth: 'Month'
            },
            contentHeight: 600,
            validRange: function (nowDate) {
                return {
                    start: nowDate
                };
            },

            dateClick: function (info) {

                $('#bookLeaveForm').trigger('reset');
                $('#leave_remaining_calc').html('');
                $('.selectpicker').selectpicker('val', '');
                var startDate = document.getElementById("startDay");

                startDate.value = (convertDateToUk(info.dateStr));

                $("#bookLeaveModalForm").modal();

            }


        }
    );


    calendar.render();

});

