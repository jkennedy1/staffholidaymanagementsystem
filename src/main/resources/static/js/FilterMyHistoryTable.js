var input, filter, table, tr, td, i;
function filterTableByAbsenceType(){

    $('#holidayStatus').val("DEFAULT");
    $('#endYear').val("");

    input = document.getElementById('absenceType');
    filter = input.value.toUpperCase();
    table = document.getElementById('leaveHistoryTable');
    tr = table.getElementsByTagName("tr");

    if(filter ==="SHOWALL"){
        showAll(tr);
        return;
    }



    filterTable(filter,tr, 5);

}

function filterTableByEndDateYear(){

    $('#holidayStatus').val("DEFAULT");
    $('#absenceType').val("DEFAULT");

    input = document.getElementById('endYear');
    filter = input.value;
    table = document.getElementById('leaveHistoryTable');
    tr = table.getElementsByTagName("tr");

    if(filter ===""){
        showAll(tr);
        return;
    }



    filterTableForDate(filter,tr, 3);

}


function filterTableByHolidayStatus(){

    $('#absenceType').val("DEFAULT");
    $('#endYear').val("");

    input = document.getElementById('holidayStatus');
    filter = input.value;
    table = document.getElementById('leaveHistoryTable');
    tr = table.getElementsByTagName("tr");

    if(filter ==="SHOWALL"){
        showAll(tr);
        return;
    }
    filterTable(filter,tr, 7);

}

function showAll(tr){
        for (i = 0; i < tr.length; i++) {
            tr[i].style.display = "";
        }
}

function filterTable(filter, tr, tdNum) {


    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[tdNum];
        if (td) {
            if (td.innerHTML.toUpperCase() === filter) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}


    function filterTableForDate(filter, tr, tdNum){

        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[tdNum];
            if (td) {
                if (getYearFromString(td.innerHTML).indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }

}

//first split separates date from string
//second split separates year from date
function getYearFromString(str){

   var firstSplit = str.split(" ");
   var secondSplit = firstSplit[0].split("/");


    return  secondSplit[2] ;
}
