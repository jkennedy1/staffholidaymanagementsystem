function convertDateToUk(date) {

    var convertedDate = new Date(date);

    var dd = convertedDate.getDate();
    var mm = convertedDate.getMonth() + 1;
    var yyyy = convertedDate.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    convertedDate = dd + '-' + mm + '-' + yyyy;

    return convertedDate;
}

function convertDateToISO(date) {

    var splitDate = date.split('-');
    var month = splitDate[1] - 1;

    return new Date(splitDate[2], month, splitDate[0]);}