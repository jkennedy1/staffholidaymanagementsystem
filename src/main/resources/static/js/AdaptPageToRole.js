jQuery(document).ready(function () {

    var $admin = $('#admin');
    var $admin2 = $('#admin2');
    var $lineManager = $('#line_manager');
    var $departmentManager = $('#department_manager');
    var $pageTitle = $('#pageTitle');
    var $welcomeText = $('#welcomeText');

    //adapts each page to the current logged in users role
    $.getJSON('/staff/dashboard/getCurrentUser', function (jd) {

        switch (jd.staffMemberRole) {


            case "ROLE_ADMIN" :
                $admin.attr("style", "");
                $admin2.attr("style", "");
                $pageTitle.text("Admin DashBoard");
                $welcomeText.text("Welcome To The Admin DashBoard");
                break;

            case "ROLE_LINE_MANAGER" :
                $lineManager.attr("style", "");
                $pageTitle.text("Line Manager DashBoard");
                $welcomeText.text("Welcome To The Line Manager DashBoard");
                break;

            case "ROLE_DEPARTMENT_MANAGER" :
                $departmentManager.attr("style", "");
                $pageTitle.text("Department Manager DashBoard");
                $welcomeText.text("Welcome To The Department Manager DashBoard");
                $lineManager.attr("style", "");
                break;

            default :
                break;
        }


    });


});