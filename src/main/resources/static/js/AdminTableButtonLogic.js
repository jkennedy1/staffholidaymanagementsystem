var $staffId,
    $staffRole,
    $staffDepartment,
    $staffLeaveEntitlement,
    $staffLeaveRemaining,
    $staffEnabled;


$('#adminTable').on('click', '.response', function () {

    $staffId = $(this).closest('tr').children('td:first-child').text();
    $staffRole = $(this).closest('tr').children('td:nth-child(2)').text();
    $staffDepartment = $(this).closest('tr').children('td:nth-child(3)').text();
    $staffLeaveEntitlement = $(this).closest('tr').children('td:nth-child(5)').text();
    $staffLeaveRemaining = $(this).closest('tr').children('td:nth-child(7)').text();
    $staffEnabled = $(this).closest('tr').children('td:nth-child(8)').text();


    if ($(this)[0].classList.contains("delete-staff")) {
        $('#enabledTextChange').text("Are you sure you want to toggle this account to: " + (($staffEnabled === 'Enabled') ? 'Disabled' : 'Enabled'));
        $('#deleteConfirmModal').modal('show');
    }

    if ($(this)[0].classList.contains("update-staff")) {
        $('#updateConfirmModal').modal('show');
    }


});


//on delete modal confirmation
$('#confirmDelete').on('click', function () {
    $.post("/admin/admin_tools/toggleEnabledStaffMember", {staffId: $staffId}, function (data) {

        if (data === 'success') {
            $("#postMessage").html("<div class='alert alert-primary text-center' role='alert'>StaffMembers Account Was Successfully Changed</div>");
            $('#deleteConfirmModal').modal('hide');
            loadAdminTable();
        } else {
            $("#postMessage").html("<div class='alert alert-danger text-center' role='alert'>StaffMembers Account Change Failed! </div>");
            $('#deleteConfirmModal').modal('hide');
        }

    });
});

//on update modal confirmation
$('#confirmUpdate').on('click', function () {
    var staffMember = {
        staffId: $staffId,
        staffMemberRole: $staffRole,
        department: $staffDepartment,
        leaveEntitlement: $staffLeaveEntitlement,
        leaveRemaining: $staffLeaveRemaining
    };


    $.ajax({
        type: "POST",
        url: "/admin/admin_tools/updateStaffMember",
        data: JSON.stringify(staffMember),
        contentType: "application/json",
        success: function (data) {
            if (data === 'success') {
                $("#postMessage").html("<div class='alert alert-primary text-center' role='alert'>StaffMember was Successfully Updated!</div>");
                $('#updateConfirmModal').modal('hide');
                loadAdminTable();
            } else {
                $("#postMessage").html("<div class='alert alert-danger text-center' role='alert'>StaffMember Update Failed!</div>");

            }

        }
    });


});


