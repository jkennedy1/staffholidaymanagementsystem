$(document).ready(function () {
    loadAdminTable();
});


function renderTable(staffList) {

    var tbody = '<tbody>';


    staffList.forEach(function (staff) {


        tbody += '<tr>';

        tbody += '<td class="text-center" contenteditable= "true">' + staff.staffMemberId + '</td>';
        tbody += '<td  class="text-center" contenteditable= "true">' + staff.staffMemberRole + '</td>';
        tbody += '<td  class="text-center" contenteditable= "true">' + staff.department + '</td>';
        tbody += '<td  class="text-center">' + staff.forename + ' ' + staff.surname + '</td>';
        tbody += '<td  class="text-center" contenteditable= "true">' + staff.leaveEntitlement.toFixed(2) + '</td>';
        tbody += '<td  class="text-center">' + (staff.leaveEntitlement - staff.leaveRemaining).toFixed(2) + '</td>';
        tbody += '<td  class="text-center" contenteditable= "true">' + staff.leaveRemaining.toFixed(2) + '</td>';
        tbody += '<td class="text-center"><strong>' + ((staff.enabled) ? 'Enabled' : 'Disabled') + '</strong></td>';
        tbody += '<td class="text-center"><button  type="button" class="btn btn-danger response delete-staff">Toggle</button></td>';
        tbody += '<td class="text-center"><button  type="button" class="btn btn-warning response update-staff">Update</button></td>';

        tbody += '</tr>';


    });

    tbody += '</tbody>';
    document.getElementById('tbody').innerHTML = tbody;

    //table sorters
    $(function () {
        $("#adminTable").tablesorter();
    });

    $(function () {
        $("#adminTable").tablesorter({sortList: [[0, 0], [1, 0]]});
    });

}


function loadAdminTable() {

    $.get("/admin/admin_tools/getAllStaff", function (allStaff) {

        renderTable(allStaff);

    });
}

