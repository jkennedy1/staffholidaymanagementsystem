$(function () {

    $.getJSON('/staff/dashboard/getCurrentUser', function (jd) {
        leaveRemainingWarningRender(jd.leaveRemaining);
    });

});

//shows warnings on dashboard for when the leave deadline is approaching
//increases from yellow to red when closer
function leaveRemainingWarningRender($leaveRemaining) {
    var $currentDate = new Date();
    // var $currentDate2 = new Date();
    var currYear = $currentDate.getFullYear();
    var $warningDate = new Date(currYear, 6, 31);
    var $cutOffDate = new Date(currYear, 7, 31);
    var $daysRemaingTillWarning = getBusinessDaysCount($currentDate, $warningDate);
    var $daysRemaingTillCutOff = getBusinessDaysCount($currentDate, $cutOffDate);


    if (($daysRemaingTillWarning - $leaveRemaining) < 14) {
        $('#leaveRemainingCutoff').html("<h4 class='text-warning'>You have: " + $daysRemaingTillWarning + " days remaining to use your leave!</h4>");

    }


    if (($daysRemaingTillCutOff - $leaveRemaining) < 14) {
        $('#leaveRemainingCutoff').html("<h3><strong class='text-danger'><u>You have: " + ($daysRemaingTillCutOff) + " days remaining to use your leave!</u></strong></h3>");
    }
}