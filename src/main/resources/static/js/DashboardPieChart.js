function piechartAjax() {

    $.getJSON('/staff/dashboard/getCurrentUser', function(jd){


        loadPieChart(jd.leaveRemaining,(jd.leaveEntitlement - jd.leaveRemaining) );
    });

}

function loadPieChart($leaveRemainingPie, $leaveTakenPie) {

    new Chart(document.getElementById("pie-chart"), {
        type: 'pie',
        data: {
            labels: ["Leave Taken", "Leave Remaining"],
            datasets: [{
                label: "Leave(days)",
                backgroundColor: [
                    "#c45850",
                    "#3e95cd"

                ],
                data: [$leaveTakenPie.toFixed(2), $leaveRemainingPie.toFixed(2)],
                borderWidth: 1,
                pointRadius: 0.3
            }]
        },
        options: {


            legend: {
                labels: {
                    // This more specific font property overrides the global property
                    fontSize: 15
                }
            },


            responsive: true,
            title: {
                display: true,
                text: 'Your Leave remaining',
                fontSize: 20
            }
        }
    });

}

$(document).ready(function () {
    piechartAjax();

});