$("#bookLeaveForm").submit(function (event) {

    event.preventDefault();
    validateFormData();


});

function validateFormData() {
    var formattedDates = addHoursToDate($('input[name=startDayType]:checked').val(), $('input[name=endDayType]:checked').val(),
                                        convertDateToISO($('#startDay').val()), convertDateToISO($('#endDay').val()));

    var formData =
        {
            holidayId: 0,
            startDateTime: formattedDates.startDate,
            endDateTime: formattedDates.endDate,
            holidayReason: $('#leaveReasonTxt').val(),
            absenceType: $('#absenceType').val(),
            managerComment: "No Comments",
            holidayStatus: "PENDING",
            altHolidayId: 0
        };

    if (formData.holidayReason === "") {
        formData.holidayReason = "N/A";
    }


    //Check date is in future and between working months 01/09 and 31/08
    if (!validateDate(formData.startDateTime, formData.endDateTime)) {

        $("#postResultDiv").html("<div class='alert alert-warning text-center' role='alert'>" +
            "Invalid Date Range, try again" + "</div>");
        $('#bookLeaveModalForm').modal('hide');
        return;
    }

    ajaxPostBooking(formData);
}


function ajaxPostBooking(formData) {


    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/staff/book_leave/bookLeave",
        data: JSON.stringify(formData),
        dataType: "text",
        success: function (result) {
            console.log(result);

            if (result === 'success') {
                $("#postResultDiv").html("<div class='alert alert-primary text-center' role='alert'>" +
                    "Booking Successful!" + "</div>");

                $('#bookLeaveModalForm').modal('hide');

                calendar.refetchEvents();

            } else if(result === 'Holiday Dates Booked'){
                $("#postResultDiv").html("<div class='alert alert-warning text-center' role='alert'>Holiday Dates are already Pending/Booked By You!</div>");
                $('#bookLeaveModalForm').modal('hide');
            }
            else {
                $("#postResultDiv").html("<div class='alert alert-danger text-center' role='alert'>" +
                    "ERROR" + "</div>");
                $('#bookLeaveModalForm').modal('hide');
            }
        }
    });
}

