var $staffId, $holidayId, $reason, $absenceType;

//Accept Leave
$('#modalAcceptRequest').on('click', function (event) {
    event.preventDefault();

    lineManagerAcceptLeave("ACCEPTED");

});


//Open Decline modal form
$('#modalDeclineRequest').on('click', function (event) {
    event.preventDefault();
    $('#answerRequestModalForm').modal('hide');

    var $declineModalForm = $('#declineModalForm')

    $declineModalForm.find('form').trigger('reset');
    $declineModalForm.modal('show')

});


//Submit simple decline if no date entered
//Else validate Alt-leave data and submit
$('#modalSubmitDecline').on('click', function (event) {
    event.preventDefault();
    var $data;

    //if dates arent set then just simple decline
    if ($('#startDay').val() === "" || $('#endDay').val() === "") {
        $data = {staffId: $staffId, holidayId: $holidayId, managerComment: $('#managerComment').val()};
        simpleDeclineHoliday($data);
        return;
    }

    //else full alt leave change
    $data = validateAltOfferFormData();

    if ($data) {
        createAltLeave($data);
    }


});

//Validate Alt leave data and attach current leave data to form for backend services
function validateAltOfferFormData() {


    var $startDate = convertDateToISO($('#startDay').val());
    var $endDate = convertDateToISO($('#endDay').val());
    var $managerComment = $('#managerComment').val();


    var startDateType = $('input[name=startDayType]:checked').val();
    var endDateType = $('input[name=endDayType]:checked').val();

    var formData =
        {
            oldHolId: $holidayId,
            staffId: $staffId,
            newStartDateTime: $startDate,
            newEndDateTime: $endDate,
            oldHolidayReason: $reason,
            oldAbsenceType: $absenceType,
            newManagerComment: $managerComment,
            newHolidayStatus: "ALTERNATIVEOFFER"
        };


    //add half day/full day hours to date
    if (startDateType === "fullDay") {

        formData.newStartDateTime.setHours(9, 0, 0, 0);
    } else {
        formData.newStartDateTime.setHours(12, 0, 0, 0);
    }
    if (endDateType === "fullDay") {

        formData.newEndDateTime.setHours(9, 0, 0, 0);
    } else {
        formData.newEndDateTime.setHours(12, 0, 0, 0);
    }


    //Check date is in future and between working months 01/09 and 31/08
    if (!validateDate(formData.newStartDateTime, formData.newEndDateTime)) {

        $("#postMessage").html("<div class='alert alert-warning text-center' role='alert'>" +
            "Leave Request must be in the future & in the current business year (1st of September to 31st of August)!" + "</div>");
        $('#declineModalForm').modal('hide');
        return;
    }

    return formData;
}


//Collect table data when button is clicked on linemanager table
$('#lineManagerTable').on('click', '.respondToRequest', function () {

    $holidayId = $(this).closest('tr').children('td:nth-child(2)').text();
    $reason = $(this).closest('tr').children('td:nth-child(6)').text();
    $absenceType = $(this).closest('tr').children('td:nth-child(7)').text();
    $staffId = $(this).closest('tr').children('td:nth-child(9)').text();


});

//change leave status to $holidayStatus value
//holidayId = current holiday selected
//staffId = current staffs holiday selected
function lineManagerAcceptLeave($holidayStatus) {

    $.ajax({
        type: "POST",
        url: "/line_manager/manage_my_staff/AcceptLeave",
        contentType: "application/json",
        data: JSON.stringify({
            holidayId: $holidayId,
            staffId: $staffId,
            holidayStatus: $holidayStatus
        }),
        dataType: "text",
        success: function (result) {

            if (result === 'success') {
                $('#answerRequestModalForm').modal("hide");
                $('#postMessage').html("<div class='alert alert-primary text-center' role='alert'>Leave Status Was Successfully Changed</div>");

                reloadLineManagerChartAndTable();
            } else {
                $('#answerRequestModalForm').modal("hide");
                $('#postMessage').html("<div class='alert alert-danger text-center' role='alert'>Error Changing Leave Status!</div>");

            }

        }
    });

}


//creates the alternative leave and sets the id to alt id on requested leave
function createAltLeave(formData) {

    $.ajax({
        type: "POST",
        url: "/line_manager/manage_my_staff/createAltLeave",
        contentType: "application/json",
        data: JSON.stringify(formData),
        dataType: "text",
        success: function (result) {

            if (result === 'success') {
                $('#declineModalForm').modal("hide");
                $('#postMessage').html("<div class='alert alert-primary text-center' role='alert'>Response was Submitted Successfully</div>");
                reloadLineManagerChartAndTable()
            } else if (result === "not enough leave remaining") {
                $('#declineModalForm').modal("hide");
                $('#postMessage').html("<div class='alert alert-danger text-center' role='alert'>Not Enough Days Remaining for Alt Leave!</div>");

            }else if(result === "Holiday Dates Booked"){
                $('#declineModalForm').modal("hide");
                $('#postMessage').html("<div class='alert alert-danger text-center' role='alert'>Holiday Dates are already Pending/Booked By Staff Member!</div>");
            } else {
                $('#declineModalForm').modal("hide");
                $('#postMessage').html("<div class='alert alert-danger text-center' role='alert'>Error Submitting Response</div>");

            }

        }
    });


}


//Just set holiday status to decline and add manager comment
function simpleDeclineHoliday(formData) {

    $.ajax({
        type: "POST",
        url: "/line_manager/manage_my_staff/simpleManagerDeclineLeave",
        contentType: "application/json",
        data: JSON.stringify(formData),
        dataType: "text",
        success: function (result) {

            if (result === 'success') {
                $('#declineModalForm').modal("hide");
                $('#postMessage').html("<div class='alert alert-primary text-center' role='alert'>Leave was Successfully Declined</div>");
                reloadLineManagerChartAndTable()
            } else {
                $('#declineModalForm').modal("hide");
                $('#postMessage').html("<div class='alert alert-danger text-center' role='alert'>Error Declining Leave</div>");

            }

        }
    });


}

//re-renders the table depending on button visibility (the view the user was last using)
function reloadLineManagerChartAndTable() {
    if ($('#nextYear').is(':visible')) {
        getLineManagerTableByYear();
        barChartAjax();
    } else {
        getLineManagerTableByMonth();
        barChartAjax();
    }
}









