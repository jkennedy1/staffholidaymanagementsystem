function loadTableButtonLogic (tableVal) {
    var $altHolidayId;
    var $currHolidayId;
    var $holidayToCancel;
    var table = tableVal;


    //td:first = altHolidayId;
    //td:nth-child(2) = holidayId;

    //load alt offer modal
    table.on('click','.viewOffer' ,function () {

        $altHolidayId = $(this).closest('tr').children('td:first').text();
        $currHolidayId = $(this).closest('tr').children('td:nth-child(2)').text();
        getOffer($altHolidayId);


    });

    //load cancel modal
    table.on('click', '.cancelRequest', function(){

        $holidayToCancel = $(this).closest('tr').children('td:nth-child(2)').text();
    });


    //cancel holiday modal request
    $("#modalCancelRequest").on('click',function (event){
        event.preventDefault();
        cancelLeave ($holidayToCancel,"CANCELLED_ARCHIVED");


    });

    //change declined leave to archived
    //change alt offer to decline archived
    $("#modalDeclineOffer").on('click', function (event){
        event.preventDefault();
        archiveDeclinedLeave("DECLINED_ARCHIVED");

        console.log("canceling");
    });

    function cancelLeave ($leaveToCancel,$holidayStatus){

        $.ajax({
            type: "POST",
            url: "/staff/dashboard/cancelLeave",
            data: {holidayToCancel : $leaveToCancel,
                holidayStatus : $holidayStatus},
            success: function (result) {

                if (result === 'success') {
                    $('#CancelRequestModalForm').modal("hide");
                    $('#ViewOfferModalForm').modal("hide");
                    $('#postMessage').html("<div class='alert alert-primary text-center' role='alert'>Leave was Successfully Cancelled!</div>");
                    reload(table);

                } else {
                    $('#CancelRequestModalForm').modal("hide");
                    $('#ViewOfferModalForm').modal("hide");
                    $('#postMessage').html("<div class='alert alert-danger text-center' role='alert'>Error Processing Cancellation Request!</div>");

                }

            }
        });

    }

    //change leave status to $holidayStatus value
    //currHoliday = current declined holiday selected
    //staffId is gathered on serverside
    //on success accept alt offer
    function archiveDeclinedLeave($holidayStatus) {

        $.ajax({
            type: "POST",
            url: "/staff/dashboard/archiveDeclinedLeave",
            contentType: "application/json",
            data: JSON.stringify({
                holidayId: $currHolidayId,
                holidayStatus: $holidayStatus
            }),
            dataType: "text",
            success: function () {
                cancelLeave($altHolidayId,"DECLINED_ALT_OFFER_ARCHIVED");
            }
        });

    }




//load alt leave data
    function getOffer($altHolidayId) {


        $.getJSON('/staff/book_leave/loadAltLeave', {altHolidayId:$altHolidayId}, function (jd) {
            var dateOptions = {day: 'numeric', month: 'numeric', year: 'numeric'};
            var timeOptions = {hour: 'numeric', minute: 'numeric', hour12: true};

            var startDateOffer = new Date(jd.startDateTime);
            var endDateOffer = new Date(jd.endDateTime);


            $('#offerStartDate').html('New Start Date: ' + startDateOffer.toLocaleString('en-GB',dateOptions) +' at '+startDateOffer.toLocaleString('en-GB', timeOptions));
            $('#offerEndDate').html('New End Date: ' + endDateOffer.toLocaleString('en-GB',dateOptions) +' at '+endDateOffer.toLocaleString('en-GB', timeOptions));
            $('#ViewOfferModalForm').modal('show');

        });
    }



    //accept alt offer modal accept
    $("#acceptOffer").on('click',function (event){
        event.preventDefault();
        submitOffer();


    });


    function submitOffer(){

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/staff/dashboard/acceptAlternativeOffer",
            data: JSON.stringify({altHolidayId : $altHolidayId,
                    currHolidayId : $currHolidayId}),
            dataType: "text",
            success: function (result) {

                    if (result === 'success') {
                        $('#ViewOfferModalForm').modal("hide");
                        $('#postMessage').html("<div class='alert alert-primary text-center' role='alert'>Offer was Successfully Accepted!</div>");
                        reload(table);

                } else {

                        $('#ViewOfferModalForm').modal("hide");
                        $('#postMessage').html("<div class='alert alert-danger text-center' role='alert'>Error Processing Offer Request!</div>");

                }

            }
        });




    }



}
//reloads table and piechart
function reload(tableId){

    if (tableId.attr('id') === 'lastFiveHolidays') {
        getLastFiveHolidays();
        piechartAjax();
    }else {
        getLeaveHistory();
    }

}

