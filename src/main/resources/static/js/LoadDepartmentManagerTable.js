$(document).ready(function () {


    $.get("/department_manager/department_manager_manage_my_department/getAllStaffForDepartmentManager", function (departmentStaff) {

        renderTable(departmentStaff);


    });

});


function renderTable(staffList) {


    var tbody = '<tbody>';


    staffList.forEach(function (staff) {


        tbody += '<tr>';

        tbody += '<td class="text-center">' + staff.staffMemberId + '</td>';
        tbody += '<td  class="text-center">' + staff.staffMemberRole + '</td>';
        tbody += '<td  class="text-center">' + staff.department + '</td>';
        tbody += '<td  class="text-center">' + staff.forename + ' ' + staff.surname + '</td>';
        tbody += '<td  class="text-center">' + staff.leaveEntitlement.toFixed(2) + '</td>';
        tbody += '<td  class="text-center">' + (staff.leaveEntitlement - staff.leaveRemaining).toFixed(2) + '</td>';
        tbody += '<td  class="text-center"><strong>' + staff.leaveRemaining.toFixed(2) + '</strong></td>';

        tbody += '</tr>';

    });

    tbody += '</tbody>';
    document.getElementById('tbody').innerHTML = tbody;

    //table sorters
    $(function () {
        $("#departmentManagerTable").tablesorter();
    });

    $(function () {
        $("#departmentManagerTable").tablesorter({sortList: [[0, 0], [1, 0]]});
    });

}


