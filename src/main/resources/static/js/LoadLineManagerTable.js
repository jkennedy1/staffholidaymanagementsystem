var dateOptions = {day: 'numeric', month: 'numeric', year: 'numeric'};
var timeOptions = {hour: 'numeric', minute: 'numeric', hour12: true};
var $hiddenPageDateValue = $('#CurrDate');
var $hiddenPageYearValue = $('#currYear');
var $lineManagerTableTitle = $('#lineManagerTableTitle');
var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

$(document).ready(function () {

    //Set inital date to this month
    var date = new Date();
    date.setHours(0);
    $hiddenPageDateValue.text(date.toISOString());
    $hiddenPageYearValue.val(date.getFullYear());

    getLineManagerTableByMonth();

    $lineManagerTableTitle.text("Showing Leave for: " + monthNames[date.getMonth()] + "/" + date.getFullYear());


});


//add 1 month to global date value
$("#nextMonth").on('click', function (event) {
    event.preventDefault();
    alterGlobalMonthValue(+1);

});
//minus 1 month to global date value
$("#prevMonth").on('click', function (event) {
    event.preventDefault();

    alterGlobalMonthValue(-1);

});

//set global month to current month
$("#returnToCurrMonth").on('click', function (event) {
    event.preventDefault();

    var $currViewDate = new Date();

    $hiddenPageDateValue.text($currViewDate.toISOString());

    $lineManagerTableTitle.text("Showing Leave for: " + monthNames[$currViewDate.getMonth()] + "/" + $currViewDate.getFullYear());

    getLineManagerTableByMonth();

});

// set global month value
//re-renders the table
function alterGlobalMonthValue($value) {

    var $currViewDate = new Date($hiddenPageDateValue.text().toString());

    $currViewDate.setMonth($currViewDate.getMonth() + $value);

    $hiddenPageDateValue.text($currViewDate.toISOString());

    $lineManagerTableTitle.text("Showing Leave for: " + monthNames[$currViewDate.getMonth()] + "/" + $currViewDate.getFullYear());

    getLineManagerTableByMonth();
}


//add 1 year to global year value
$("#nextYear").on('click', function (event) {
    event.preventDefault();
    alterGlobalYearValue(+1);

});
//minus 1 year to global year value
$("#prevYear").on('click', function (event) {
    event.preventDefault();

    alterGlobalYearValue(-1);

});

//set global month to current month
$("#returnToCurrYear").on('click', function (event) {
    event.preventDefault();
    var $currViewDate = new Date();

    $hiddenPageYearValue.val($currViewDate.getFullYear());

    $lineManagerTableTitle.text("Showing Leave for: " + $currViewDate.getFullYear());

    getLineManagerTableByYear();

});


//set global Year value from param
//re-renders the table
function alterGlobalYearValue($value) {

    $hiddenPageYearValue.val(parseInt($hiddenPageYearValue.val()) + $value);


    $lineManagerTableTitle.text("Showing Leave for: " + $hiddenPageYearValue.val());

    getLineManagerTableByYear();

}


//show year buttons and hide month ones
$("#yearView").on('click', function (event) {
    event.preventDefault();

    var $returnToCurrYear = $('#returnToCurrYear');

    $('#nextMonth').attr("style", "display:none;");
    $('#prevMonth').attr("style", "display:none;");
    $('#returnToCurrMonth').attr("style", "display:none;");


    $('#nextYear').attr("style", "");
    $('#prevYear').attr("style", "");
    $returnToCurrYear.attr("style", "");

    //reset month/year
    $returnToCurrYear.click();
});


//show month buttons and hide year ones
$("#monthView").on('click', function (event) {
    event.preventDefault();
    var $returnToCurrMonth = $('#returnToCurrMonth');

    $('#nextYear').attr("style", "display:none;");
    $('#prevYear').attr("style", "display:none;");
    $('#returnToCurrYear').attr("style", "display:none;");

    $('#nextMonth').attr("style", "");
    $('#prevMonth').attr("style", "");
    $returnToCurrMonth.attr("style", "");

    //reset year
    $returnToCurrMonth.click();

});


//Gets date from global month value
//sets start to beginning of month and end to end of month
//return formatted startDate & endDate
function getDateToLoadByMonth() {

    var currDate = new Date($hiddenPageDateValue.text());


    var startDate = new Date(currDate.getFullYear(), currDate.getMonth(), 1, currDate.getHours() + 1);
    var endDate = new Date(currDate.getFullYear(), currDate.getMonth() + 1, 0, currDate.getHours() + 1);


    sessionStorage.setItem('startDate', startDate.toISOString());
    sessionStorage.setItem('endDate', endDate.toISOString());


    return {startDate: sessionStorage.getItem('startDate'), endDate: sessionStorage.getItem('endDate')}
}


//Gets date from global year value
//returns year
function getDateToLoadByYear() {

    sessionStorage.setItem('viewYear', $hiddenPageYearValue.val());

    return {year: sessionStorage.getItem('viewYear')};
}

//get all line manager staff and their holidays for specific month
function getLineManagerTableByMonth() {
    $.get("/line_manager/line_manager_my_staff/getLineManagerTableLeaveByMonth", getDateToLoadByMonth(), function (staffList) { // get from controller
        renderTable(staffList);
    });

}

//get all line manager staff and their holidays for specific year
function getLineManagerTableByYear() {
    $.get("/line_manager/line_manager_my_staff/getLineManagerTableLeaveByYear", getDateToLoadByYear(), function (staffList) { // get from controller
        renderTable(staffList);
    });

}

//renders table from a list of staff
function renderTable(staffList) {
    var dateOptions = {day: 'numeric', month: 'numeric', year: 'numeric'};
    var timeOptions = {hour: 'numeric', minute: 'numeric', hour12: true};

    var tbody = '<tbody>';


    staffList.forEach(function (staff) {

        staff.staffHolidays.forEach(function (holiday) {

            var $startDateTime = new Date(holiday.startDateTime);
            var $endDateTime = new Date(holiday.endDateTime);

            tbody += '<tr>';
            tbody += '<td hidden>' + holiday.altHolidayId + '</td>';
            tbody += '<td hidden>' + holiday.holidayId + '</td>';
            tbody += '<td>' + staff.forename + ' ' + staff.surname + '</td>';
            tbody += '<td>' + $startDateTime.toLocaleString('en-GB', dateOptions) + ' at ' + $startDateTime.toLocaleString('en-GB', timeOptions) + '</td>';
            tbody += '<td>' + $endDateTime.toLocaleString('en-GB', dateOptions) + ' at ' + $endDateTime.toLocaleString('en-GB', timeOptions) + '</td>';
            tbody += '<td>' + holiday.holidayReason + '</td>';
            tbody += '<td>' + holiday.absenceType + '</td>';
            tbody += '<td>' + holiday.holidayStatus + '</td>';
            tbody += '<td hidden>' + staff.staffMemberId + '</td>';

            switch (holiday.holidayStatus) {

                case "PENDING":
                    tbody += '<td>' + '<button  type="button" class="btn btn-danger respondToRequest" data-toggle="modal" data-target="#answerRequestModalForm">' + 'Respond' + '</button>' + '</td>';
                    break;

                case "ACCEPTED":
                    tbody += '<td>' + '<button  type="button" class="btn btn-primary"  >' + 'N/A' + '</button>' + '</td>';
                    break;

            }

            tbody += '</tr>'

        });
        tbody += '</tbody>';
        document.getElementById('tbody').innerHTML = tbody;

    });

}


