
$(document).ready(function () {
    getLeaveHistory();
    loadTableButtonLogic($('#leaveHistoryTable'));


});

function getLeaveHistory() {

    $.get("/staff/my_leave_history/getStaffLeaveHistory", function (response) { // get from controller
        var dateOptions = {day: 'numeric', month: 'numeric', year: 'numeric'};
        var timeOptions = {hour: 'numeric', minute: 'numeric', hour12: true};
            var tbody = '<tbody>';

            for (var i = 0; i < response.length; i++) {

                var $startDateTime = new Date(response[i].startDateTime);
                var $endDateTime = new Date(response[i].endDateTime);

                tbody += '<tr>';
                tbody += '<td hidden>' + response[i].altHolidayId + '</td>';
                tbody += '<td hidden>' + response[i].holidayId + '</td>';
                tbody += '<td>' + $startDateTime.toLocaleString('en-GB', dateOptions) + ' at ' + $startDateTime.toLocaleString('en-GB', timeOptions) + '</td>';
                tbody += '<td>' + $endDateTime.toLocaleString('en-GB', dateOptions) + ' at ' + $endDateTime.toLocaleString('en-GB', timeOptions) + '</td>';
                tbody += '<td>' + response[i].holidayReason + '</td>';
                tbody += '<td>' + response[i].absenceType + '</td>';
                tbody += '<td>' + response[i].managerComment + '</td>';
                tbody += '<td>' + response[i].holidayStatus + '</td>';

                switch (response[i].holidayStatus) {

                    case "PENDING":
                        tbody += '<td>' + '<button  type="button" class="btn btn-danger cancelRequest" data-toggle="modal" data-target="#CancelRequestModalForm">' + 'Cancel' + '</button>' + '</td>';
                        break;

                    case "ACCEPTED":
                        tbody += '<td>' + '<button  type="button" class="btn btn-danger cancelRequest" data-toggle="modal" data-target="#CancelRequestModalForm">' + 'Cancel' + '</button>' + '</td>';
                        break;

                    case "DECLINED":
                        if (response[i].altHolidayId !== 0) {
                            tbody += '<td>' + '<button  type="button" class="btn btn-primary viewOffer" data-toggle="modal" data-target="#ViewOfferModalForm" >' + 'View Offer' + '</button>' + '</td>';
                        } else {
                            tbody += '<td>' + '<button  type="button" class="btn btn-primary " disabled >' + 'N/A' + '</button>' + '</td>';
                        }
                        break;

                    default
                    :
                        tbody += '<td>' + '<button  type="button" class="btn btn-primary " disabled >' + 'N/A' + '</button>' + '</td>';
                        break;
                }

                tbody += '</tr>'

            }
            tbody += '</tbody>';
            document.getElementById('tbody').innerHTML = tbody;

        }
    )
    ;


}