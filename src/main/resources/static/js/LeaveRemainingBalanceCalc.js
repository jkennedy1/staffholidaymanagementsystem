$('#bookLeaveModalForm').on('show.bs.modal', function () {
    getLeaveRemaining();

});

//adds hours to date based off fullday/half day radio button inputs
function addHoursToDate(startDateType, endDateType, startDayDate, endDayDate) {


    if (startDateType === "fullDay") {

        startDayDate.setHours(9, 0, 0, 0);
    } else {
        startDayDate.setHours(12, 0, 0, 0);
    }
    if (endDateType === "fullDay") {

        endDayDate.setHours(9, 0, 0, 0);
    } else {
        endDayDate.setHours(12, 0, 0, 0);
    }


    return {startDate: startDayDate, endDate: endDayDate}

}
//count working days
//-1 if end date is a weekend as end date is not counted
function getBusinessDaysCount(startDate, endDate) {

    var count = 0;
    var curDate = new Date(startDate);
    while (curDate < endDate) {
        var dayOfWeek = curDate.getDay();
        if (!((dayOfWeek === 6) || (dayOfWeek === 0)))
            count++;
        curDate.setDate(curDate.getDate() + 1);
    }

    if (curDate.getDay() === 6 || curDate.getDay() === 0) {
        count -= 1;
    }

    return count;

}


function updateLeaveRemainingBalanceOnModal() {

    var dates = addHoursToDate($('input[name=startDayType]:checked').val(), $('input[name=endDayType]:checked').val(), convertDateToISO($('#startDay').val()), convertDateToISO($('#endDay').val()));
    var businessDays = getBusinessDaysCount(dates.startDate, dates.endDate);
    var businessHours = businessDays * 8; // = 8 hours/day

    //member has worked for 3 hours on start date so minus 3
    if (dates.startDate.getHours() === 12) {
        businessHours -= 3.00;
    }
    //member has 3 hours off on return date so add 3
    if (dates.endDate.getHours() === 12) {
        businessHours += 3.00;
    }

    businessHours /= 8.;


    var $button = $('#bookLeaveButton');
    var currDate = new Date();


    //Date Validation
    if (isNaN(dates.startDate) || isNaN(dates.endDate)) {
        $('#leave_remaining_calc').html("<Strong style='color:red'>Please enter a valid date!</Strong>");
        $button.addClass("disabled");
        $button.attr("aria-disabled", 'true');
        $button.prop('disabled', true);
    } else if (($leaveRemaining - businessHours).toFixed(2) < 0) {
        $('#leave_remaining_calc').html("<Strong style='color:red'>You do not have enough balance!</Strong>");
        $button.addClass("disabled");
        $button.attr("aria-disabled", 'true');
        $button.prop('disabled', true);
    } else if (dates.startDate.getMonth() === currDate.getMonth() && dates.startDate.getDate() < currDate.getDate()) {
        $('#leave_remaining_calc').html("<Strong style='color:red'>Start date needs to be in the future!</Strong>");
        $button.addClass("disabled");
        $button.attr("aria-disabled", 'true');
        $button.prop('disabled', true);
    } else {
        $('#leave_remaining_calc').html("Leave balance after booking: " + ($leaveRemaining - businessHours).toFixed(2) + "days");
        $button.removeClass("disabled");
        $button.attr("aria-disabled", 'false');
        $button.prop('disabled', false);
    }
}


$('.modal-date').on('focusout', function () {

    updateLeaveRemainingBalanceOnModal()

});

$('[name="startDayType"]').on('change', function () {


    updateLeaveRemainingBalanceOnModal()

});

$('[name="endDayType"]').on('change', function () {

    updateLeaveRemainingBalanceOnModal()

});












