$(document).ready(function () {
    $.get("/admin/admin_add_staff/getLineManagers", function (lineManagerList) {


        lineManagerList.forEach(function (lineManager) {
            $('#lineManager').append($('<option>', {
                value: lineManager.staffMemberId,
                text: lineManager.forename+" "+lineManager.surname + ", ID:(" + lineManager.staffMemberId + ")"
            }));
        });

    });
});


$('#addStaffMember').on('submit', function (event) {
    event.preventDefault();

    var staff = {
        forename: $('#forename').val(),
        surname: $('#surname').val(),
        password: $('#password').val(),
        department: $('#department').val().toUpperCase(),
        eMail: $('#eMail').val(),
        leaveEntitlement: $('#leaveEntitlement').val(),
        role: $('#staffRole').val(),
        lineManagerId: $('#lineManager').val()

    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/admin/admin_add_staff/addStaff",
        data: JSON.stringify(staff),
        dataType: "text",
        success: function (response) {
            if (response !== 0) {
                $("#postMessage").html("<div class='alert alert-primary text-center' role='alert'>Staff Member Successfully Added to Database: Staff ID = " + response + "</div>");
                $('#addStaffMember')[0].reset();
            } else {
                $("#postMessage").html("<div class='alert alert-danger text-center' role='alert'>Failed to Add Staff To Database!</div>");
                $('#addStaffMember')[0].reset();
            }
        }


    });

});