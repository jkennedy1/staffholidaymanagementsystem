function barChartAjax() {
    var $data = [];
    $.getJSON('/line_manager/line_manager_my_staff/getAllStaffForLineManager', function (jd) {

        jd.forEach(function (staff) {

            $data.push({
                staffName: staff.forename + ' ' + staff.surname,
                staffLeaveEntitlement: staff.leaveEntitlement.toFixed(2),
                staffLeaveRemaining: staff.leaveRemaining.toFixed(2)
            });

        });
        loadBarChart($data);

    });

}


function populateDatasets($staffArray) {

    for (var i = 0; i < $staffArray.length; i++) {

        chartStaffNames.datasets[0].data.push($staffArray[i].staffLeaveRemaining);
        chartStaffNames.datasets[1].data.push($staffArray[i].staffLeaveEntitlement);
        chartStaffNames.labels.push($staffArray[i].staffName);
    }

}

var chartStaffNames;
var chartLeaveRemaining;
var chartLeaveEntitlement;

function loadBarChart($staffArray) {


    chartLeaveEntitlement = {
        label: 'Leave Entitlement (days)',
        data: [],
        backgroundColor: "#c45850",
        yAxisID: "y-axis-days"
    };
    chartLeaveRemaining = {
        label: 'Leave Remaining (days)',
        data: [],
        backgroundColor: "#3e95cd",
        yAxisID: "y-axis-days"
    };

    chartStaffNames = {
        labels: [],
        datasets: [chartLeaveRemaining, chartLeaveEntitlement]
    };

    populateDatasets($staffArray);

    var chartOptions = {

        scales: {
            yAxes: [{
                id: "y-axis-days",
                ticks: {beginAtZero: true, stepSize: 5},
                stacked: false,
                scaleLabel: {display: true, labelString: 'days'},
                scaleSteps: 0.5
            }],
            xAxes: [{
                ticks: {min: 0}, stacked: true
            }]
        },
        responsive: true,
        title: {
            display: true,
            text: 'Staff Leave',
            fontSize: 20
        }

    };


    new Chart(document.getElementById("chart"), {
        type: 'bar',
        data: chartStaffNames,
        options: chartOptions
    });

}

$(document).ready(function () {
    barChartAjax();

});