/*Delete all tables for fresh Database on run. Testing purposes*/
SET foreign_key_checks = 0;
DROP TABLE IF EXISTS staff_member;
DROP TABLE IF EXISTS holiday;
DROP EVENT IF EXISTS reset_leave;
DROP EVENT IF EXISTS archive_leave;
SET foreign_key_checks = 1;

/*Sets timezone to System, fixes bug with java localDatetime + set event scheduler to on */
SET GLOBAL time_zone = 'SYSTEM';
SET GLOBAL event_scheduler = ON;
/* Table creation */


CREATE TABLE staff_member (
    staff_member_id INT AUTO_INCREMENT NOT NULL,
    staff_member_role VARCHAR(255) NOT NULL,
    staff_password VARCHAR(255) NOT NULL,
    forename VARCHAR(255) NOT NULL,
    surname VARCHAR(255) NOT NULL,
    department VARCHAR(3) NOT NULL,
    line_manager_staff_id INT,
    e_mail VARCHAR(320) NOT NULL,
    leave_entitlement FLOAT NOT NULL,
    leave_remaining FLOAT NOT NULL,
    enabled BOOLEAN NOT NULL,
    PRIMARY KEY (staff_member_id)
);



CREATE TABLE holiday (
    holiday_id INT AUTO_INCREMENT NOT NULL,
    staff_member_id INT NOT NULL,
    start_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    end_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    holiday_reason VARCHAR(280) NOT NULL,
    absence_type VARCHAR(30) NOT NULL,
    manager_comment VARCHAR(280),
    holiday_status VARCHAR(30) NOT NULL,
    alt_holiday_id INT,
    CONSTRAINT PK_Holiday PRIMARY KEY (holiday_id , staff_member_id)
);

  

/* Add FOREIGN KEY constraints to tables  */
        ALTER TABLE staff_member
		ADD CONSTRAINT FOREIGN KEY (line_manager_staff_id) REFERENCES staff_member (staff_member_id);
        
		ALTER TABLE holiday
		ADD CONSTRAINT FOREIGN KEY (staff_member_id) REFERENCES staff_member (staff_member_id);
        
        
        
/*Event to reset Leave values at the start of the academic year*/
DELIMITER $$
CREATE EVENT IF NOT EXISTS reset_leave
    ON SCHEDULE EVERY 1 YEAR STARTS '2019-08-01 00:00:00'
    DO 
		BEGIN
			UPDATE staff_member
				SET leave_remaining = (
											CASE 
												WHEN
													(leave_remaining > 0.00)
													THEN
													(
														CASE
															WHEN 
																((leave_entitlement + leave_remaining) > (leave_entitlement+5))
														
																	THEN 
															
																		(leave_entitlement+5)
															
																	ELSE
																		(leave_entitlement+leave_remaining)
														END
												
													)
												ELSE 
													leave_entitlement
				
												END
											);
		END$$
       DELIMITER ; 
       
       
/*Event to change leave status values to archived when end date has passed*/
DELIMITER $$
CREATE EVENT IF NOT EXISTS archive_leave
	ON SCHEDULE EVERY 6 HOUR
    DO
		BEGIN
			UPDATE holiday
				SET holiday_status = ( CASE
										WHEN( NOW() > holiday.end_date)
											THEN('ARCHIVED')
											ELSE(holiday_status) 
										END)
			WHERE holiday_status = 'ACCEPTED';

END$$
DELIMITER ;
		
   
/*Insert Test Data*/

INSERT INTO staff_member(staff_member_role, staff_password, forename, surname, department, line_manager_staff_id, e_mail,  leave_entitlement, leave_remaining, enabled)
	VALUES ('ROLE_DEPARTMENT_MANAGER', '$2a$10$AJ2rm1S8L9jzKoC3Uk4V5upOaP3jvBpn8tsSZ/41zU8jYqJlFBqu.', 'John', 'Kennedy', 'CMP', NULL, 'Kenndey27@hotmail.com', 32.00, 12.0, true),
           ('ROLE_LINE_MANAGER', '$2a$10$GNdbceswM68nfGTmAv7Ji.Et1Nmb.ie/ordBqxx6bw/kMwOH8P.bq', 'Peter', 'Griffin', 'CMP', 1, 'Griffin27@hotmail.com', 32.03, 11.01, true),
           ('ROLE_STAFF', '$2a$10$rPx8LSewS7R0BZOq9rGo4umh87.bU.JvSiGqoxWWzJ2xN9MVYagQG', 'Josh', 'Nobody', 'CMP', 2, 'Nobody27@hotmail.com', 25.00, 14.00, true),
		   ('ROLE_ADMIN', '$2a$10$X4g4sVPsltgLF4euZLtcu.tpUEBxtSu4PxLlymHuG5UZQU2W3Vy5y', 'Mark', 'Someone', 'CMP', 2, 'Someone27@hotmail.com', 20.00, 5.00, true);
           
           
INSERT INTO holiday(staff_member_id, start_date, end_date, holiday_reason, absence_type, manager_comment, holiday_status, alt_holiday_id)
	VALUES  (3, '2019-02-27 12:00:00' , '2019-02-28 09:00:00', 'N/A' ,'UNPAIDLEAVE', 'No Comments', 'ARCHIVED', 0),
            (3, '2020-03-28 12:00:00' , '2020-03-30 09:00:00', 'N/A' ,'PAIDLEAVE', 'No Comments', 'PENDING', 0),
			(3, '2019-04-28 09:00:00' , '2019-04-30 12:00:00', 'Going to Wales' ,'VACATIONLEAVE', 'No Comments', 'ACCEPTED', 0),
			(3, '2019-05-28 12:00:00' , '2019-05-30 09:00:00', 'Going to Manchester' ,'VACATIONLEAVE', 'No Comments', 'ACCEPTED', 0),
			(3, '2019-06-28 09:00:00' , '2019-06-30 09:00:00', 'Going to Liverpool' ,'VACATIONLEAVE','No Comments', 'PENDING', 0),
			(3, '2019-07-28 09:00:00' , '2019-07-30 09:00:00', 'Going to Margate' ,'VACATIONLEAVE','No Comments', 'PENDING', 0),
			(3, '2019-08-28 09:00:00' , '2019-08-30 12:00:00', 'Going to York' ,'VACATIONLEAVE', 'Too many leave requests that week', 'DECLINED',8),
			(3, '2019-09-27 09:00:00' , '2019-09-30 09:00:00', 'Going to York' ,'VACATIONLEAVE', 'Alternative offer', 'ALTERNATIVEOFFER', 0),
			(3, '2019-10-28 09:00:00' , '2019-10-30 09:00:00', 'N/A' ,'MATERNITYLEAVE', 'No Comments', 'PENDING',0),
            (3, '2019-08-28 09:00:00' , '2019-08-30 12:00:00', 'Going to York' ,'VACATIONLEAVE', 'Testing the waters', 'DECLINED',0);
     
            
           

        
        
			

            
            
         