package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class EncodePasswordTester {


    public static void main(String[] args){

        String password = "123456789";
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);

        System.out.println(hashedPassword);



    }
}
