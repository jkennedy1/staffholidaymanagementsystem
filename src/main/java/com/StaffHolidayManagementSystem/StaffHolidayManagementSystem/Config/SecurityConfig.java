package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/** Spring Security Config
 * @author John Kennedy
 * @version 1.0
 */

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JdbcTemplate jdbcTemplate;

    public SecurityConfig(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //Authentication of users, using database tables
    //gets id and password from staff table
    //gets is and role from staff table
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authenticationMgr) throws Exception {

        authenticationMgr.jdbcAuthentication().dataSource(jdbcTemplate.getDataSource())
                .passwordEncoder(new BCryptPasswordEncoder())
                .usersByUsernameQuery("SELECT staff_member_id, staff_password, enabled FROM staff_member WHERE staff_member_id=?")
                .authoritiesByUsernameQuery("SELECT staff_member_id, staff_member_role FROM staff_member WHERE staff_member_id=?");


    }


    //Authorization of API requests
    //login and security session data
    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                .antMatchers("/resources/*", "/js/*", "/webjars/*").permitAll()
                .antMatchers("/staff/*").hasAnyRole("STAFF", "LINE_MANAGER", "DEPARTMENT_MANAGER", "ADMIN")
                .antMatchers("/line_manager/*").hasAnyRole("LINE_MANAGER", "DEPARTMENT_MANAGER")
                .antMatchers("/department_manager/*").hasAnyRole("LINE_MANAGER","DEPARTMENT_MANAGER")
                .antMatchers("/admin/*").hasRole("ADMIN")
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .defaultSuccessUrl("/default", true)
                .and()
                .logout()
                .permitAll()
                .and()
                .csrf()
                .disable();

    }


}


