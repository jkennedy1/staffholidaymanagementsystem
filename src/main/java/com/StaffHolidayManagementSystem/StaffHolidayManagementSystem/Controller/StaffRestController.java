package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Controller;


import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffMember;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

/** Staff Rest API
 * @author John Kennedy
 * @version 1.0
 */

@RestController
public class StaffRestController {

    @Autowired
    private final StaffService staffService;

    public StaffRestController(StaffService staffService) {
        this.staffService = staffService;
    }


    /**
     * gets staff member by id
     * @param id StaffMembers to return
     * @return StaffMember object
     */
    @RequestMapping(value = "/staff/dashboard/getStaffById", method = RequestMethod.GET, produces = "application/json")
    public StaffMember getStaffById(@RequestParam int id) {

        return staffService.getStaffMemberById(id);

    }

    /**
     * Uses spring security login ID to retrieve StaffMember
     * @param principal staffId
     * @return StaffMember object
     */
    @RequestMapping(value = "/staff/dashboard/getCurrentUser", method = RequestMethod.GET, produces = "application/json")
    public StaffMember getCurrentUser(Principal principal) {


        return staffService.getStaffMemberById(Integer.parseInt(principal.getName()));

    }

    /**
     * Gets list of staff members who belong to calling line manager populated with holidays between startDate and endDate
     * @param startDate the date to look from (only month will be used)
     * @param endDate the date to look till (only month will be used)
     * @param currentUser the date
     * @return List of StaffMembers
     */
    @RequestMapping(value = "/line_manager/line_manager_my_staff/getLineManagerTableLeaveByMonth", method = RequestMethod.GET, produces = "application/json")
    public List<StaffMember> getLineManagerTableLeaveByMonth(@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate, @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate, @AuthenticationPrincipal UserDetails currentUser) {


        return staffService.getAllStaffByLineManagerByMonth(Integer.parseInt(currentUser.getUsername()), startDate, endDate);
    }

    /**
     * Gets list of staff members who belong to calling line manager populated with holidays between startDate and endDate
     * @param year the year to get holidays from (by startDate)
     * @param currentUser the line manager id calling the method
     * @return List of StaffMembers
     */
    @RequestMapping(value = "/line_manager/line_manager_my_staff/getLineManagerTableLeaveByYear", method = RequestMethod.GET, produces = "application/json")
    public List<StaffMember> getLineManagerTableLeaveByYear(@RequestParam("year") int year, @AuthenticationPrincipal UserDetails currentUser) {


        return staffService.getAllStaffByLineManagerByYear(Integer.parseInt(currentUser.getUsername()), year);
    }

    /**
     * Gets list of staff members who belong to calling line manager populated with all their holidays
     * @param currentUser the line manager id calling the method
     * @return List of StaffMembers
     */
    @RequestMapping(value = "/line_manager/line_manager_my_staff/getAllStaffForLineManager", method = RequestMethod.GET, produces = "application/json")
    public List<StaffMember> getAllStaffForLineManager(@AuthenticationPrincipal UserDetails currentUser) {


        return staffService.getAllStaffByLineManager(Integer.parseInt(currentUser.getUsername()));

    }

    /**
     * Gets list of staff members who belong to calling department manager's department
     * @param currentUser the calling department manager's id
     * @return List of StaffMembers
     */
    @RequestMapping(value = "/department_manager/department_manager_manage_my_department/getAllStaffForDepartmentManager", method = RequestMethod.GET, produces = "application/json")
    public List<StaffMember> getAllStaffForDepartmentManager(@AuthenticationPrincipal UserDetails currentUser) {


        return staffService.getAllStaffByDepartment(Integer.parseInt(currentUser.getUsername()));

    }

    /**
     * Gets all staff but the calling admin
     * @param currentUser the calling admin's ID
     * @return List of StaffMembers
     */
    @RequestMapping(value = "/admin/admin_tools/getAllStaff", method = RequestMethod.GET, produces = "application/json")
    public List<StaffMember> getAllStaff(@AuthenticationPrincipal UserDetails currentUser) {


        return staffService.getAllStaff(Integer.parseInt(currentUser.getUsername()));

    }

    /**
     * Toggle StaffMembers account status between true(activated) and false(de-activated)
     * @param currentUser the calling admin's ID
     * @param staffId the staff whos account to change
     * @return "success" if staff updated or "error" if failed
     */
    @RequestMapping(value = "/admin/admin_tools/toggleEnabledStaffMember", method = RequestMethod.POST)
    public String toggleEnabledStaffById(@AuthenticationPrincipal UserDetails currentUser, @RequestParam("staffId") int staffId) {



        return staffService.toggleEnabledStaffById(Integer.parseInt(currentUser.getUsername()), staffId);

    }

    /**
     * Updates StaffMembers role,department,leaveEntitlement, leaveRemaining by their staff ID
     * @param jsonString Json String containing role,department,leaveEntitlement, leaveRemaining, staffId
     * @param currentUser the calling admin's ID
     * @return "success" if staff updated or "error" if failed
     */
    @RequestMapping(value = "/admin/admin_tools/updateStaffMember", method = RequestMethod.POST)
    public String updateStaffById(@RequestBody String jsonString, @AuthenticationPrincipal UserDetails currentUser) {



        return staffService.updateStaffById(Integer.parseInt(currentUser.getUsername()), jsonString);

    }

    /**
     * Get a list of all Line Managers
     * @return List of all Line Managers
     */
    @RequestMapping(value = "/admin/admin_add_staff/getLineManagers", method = RequestMethod.GET, produces = "application/json")
    public List<StaffMember> getAllLineManagers() {


        return staffService.getAllLineManagers();

    }

    /**
     * Add Staff Member to system from jsonString
     * @param jsonString jsonString of Staff Member (staffMemberRole,department,leaveEntitlement,leaveRemaining)
     * @return id of created Staff Member
     */
    @RequestMapping(value = "/admin/admin_add_staff/addStaff", method = RequestMethod.POST)
    public int addStaff(@RequestBody String jsonString) {



        return staffService.addStaff(jsonString) ;

    }










}
