package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Controller;



import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/** Web Portal API
 * This class deals out static web pages to users who consume the API
 * @author John Kennedy
 * @version 1.0
 */
@Controller
public class WebPortalController {

    // Staff Page Controllers
    @RequestMapping("/staff/dashboard")
    public String getStaffDashboardPage() {

        return ("staff_dashboard");
    }

    @GetMapping("/staff/my_leave_history")
    public String getMyLeaveHistoryPage() {
        return ("my_leave_history");
    }

    @GetMapping("/staff/book_leave")
    public String getBookLeavePage() {
        return ("book_leave");
    }




    //Line Manager Page Controllers
    @RequestMapping("/line_manager/manage_my_staff")
    public String getLineManagerManageMyStaff() {

        return ("line_manager_manage_my_staff");
    }




    //Department Manager Page Controllers
    @RequestMapping("/department_manager/department_manager_manage_my_department")
    public String getDepartmentManagerManagementPage() {

        return ("department_manager_manage_my_department");
    }




    //Admin Page Controllers
    @RequestMapping("/admin/admin_tools")
    public String getAdminToolsPage() {

        return ("admin_tools");
    }
    @RequestMapping("/admin/admin_add_staff")
    public String getAdminAddStaffPage() {

        return ("admin_add_staff");
    }


}
