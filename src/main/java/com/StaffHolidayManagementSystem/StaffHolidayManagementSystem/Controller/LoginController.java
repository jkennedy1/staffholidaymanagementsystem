package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/** Holiday Login API
 * @author John Kennedy
 * @version 1.0
 */

@Controller
public class LoginController {

    /**
     * redirects to login page
     * @return login.html
     */
    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    /**
     * used by spring security
     * @param model DOM to be manipulated
     * @return login.html with error
     */
    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login.html";
    }


    /**
     * on successful login redirect to webportal controller
     * @param request used by Spring-Security
     * @return redirect to webPortalController
     */
    @RequestMapping("/default")
    public String defaultLoginProcess(HttpServletRequest request){
            return "redirect:/staff/dashboard";
    }




}
