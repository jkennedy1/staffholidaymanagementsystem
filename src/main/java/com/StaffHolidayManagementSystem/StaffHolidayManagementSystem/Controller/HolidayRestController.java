package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Controller;


import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.HolidayStatus;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffHoliday;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Service.HolidayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

/** Holiday Rest API
 * @author John Kennedy
 * @version 1.0
 */

@RestController
public class HolidayRestController {

    private final
    HolidayService holidayService;



    @Autowired
    public HolidayRestController(HolidayService holidayService) {
        this.holidayService = holidayService;
    }


    /**
     *
     * @param startDate The date from which the holidays will be gathered
     * @param endDate the date with which the holiday list will gathered until
     * @param principal spring security ID taken from logged in user
     * @return List of staffHolidays where startDate is between startDate and endDate params
     */
    @RequestMapping(value = "/staff/book_leave/getLeaveEventsForCalendar", method = RequestMethod.GET, produces = "application/json")
    public List<StaffHoliday> getLeaveEventsForCalendar(@RequestParam("start") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
                                                        @RequestParam("end") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
                                                        Principal principal) {


        return holidayService.getLeaveEventsForCalendar(Integer.parseInt(principal.getName()), startDate, endDate);

    }

    /**
     *
     * @param jsonHoliday holiday string in Json format
     * @param principal spring security ID taken from logged in user
     * @return "success" or "error" depending if method was successful
     */
    @RequestMapping(value = "/staff/book_leave/bookLeave", method = RequestMethod.POST)
    public String bookLeave(@RequestBody String jsonHoliday, Principal principal) {

        return holidayService.addHolidayByJsonString(jsonHoliday, Integer.parseInt(principal.getName()));
    }

    /**
     *
     * @param holidayId id of the holiday to cancel
     * @param principal spring security ID taken from logged in user
     * @param holidayStatus the status you wish to set the declined holiday to i.e HolidayStatus.DECLINED_ALT_OFFER_ARCHIVED
     * @return "success" or "error" depending if method was successful
     */
    @RequestMapping(value = "/staff/dashboard/cancelLeave", method = RequestMethod.POST)
    public String cancelLeave(@RequestParam("holidayToCancel") int holidayId, Principal principal, @RequestParam("holidayStatus") String holidayStatus) {


        return holidayService.declineLeave(holidayService.getHolidayById(holidayId, Integer.parseInt(principal.getName())), Integer.parseInt(principal.getName()), HolidayStatus.fromValue(holidayStatus));
    }

    /**
     *
     * @param altHolidayId id of alternative holiday to load
     * @param principal spring security ID taken from logged in user
     * @return The altHoliday requested
     */
    @RequestMapping(value = "/staff/book_leave/loadAltLeave", method = RequestMethod.GET, produces = "application/json")
    public StaffHoliday loadAltLeave(@RequestParam("altHolidayId") int altHolidayId, Principal principal) {


        return holidayService.getHolidayById( altHolidayId, Integer.parseInt(principal.getName()));
    }

    /**
     *
     * @param altOfferJson contains altHolidayID and CurrHolidayID in Json format to process request
     * @param principal spring security ID taken from logged in user
     * @return "success" or "error" depending if method was successful
     */
    @RequestMapping(value = "/staff/dashboard/acceptAlternativeOffer", method = RequestMethod.POST)
    public String acceptAlternativeOffer(@RequestBody String altOfferJson, Principal principal) {


        return holidayService.acceptAlternativeOffer(altOfferJson, Integer.parseInt(principal.getName()));
    }

    /**
     *
     * @param currentUser spring security ID taken from logged in user
     * @return List of last five holidays the user created
     */
    @RequestMapping(value = "/staff/dashboard/lastFiveHolidays", method = RequestMethod.GET, produces = "application/json")
    public List<StaffHoliday> getLastFiveHolidays(@AuthenticationPrincipal UserDetails currentUser) {

        return holidayService.getLastFiveNoneArchiveHolidaysByStaffId(Integer.parseInt(currentUser.getUsername()));
    }

    /**
     *
     * @param currentUser spring security ID taken from logged in user
     * @return List of all holidays for the user
     */
    @RequestMapping(value = "/staff/my_leave_history/getStaffLeaveHistory", method = RequestMethod.GET, produces = "application/json")
    public List<StaffHoliday> getLeaveHistory(@AuthenticationPrincipal UserDetails currentUser) {


        return holidayService.getStaffLeaveHistoryByStaffId(Integer.parseInt(currentUser.getUsername()));
    }

    /**
     *
     * @param jsonLeaveString contains currHolidayId and holidayStatus
     * @param currentUser spring security ID taken from logged in user
     * @return  "success" or "error" depending if method was successful
     */
    @RequestMapping(value = "/staff/dashboard/archiveDeclinedLeave", method = RequestMethod.POST)
    public String archiveDeclineLeave(@RequestBody String jsonLeaveString,@AuthenticationPrincipal UserDetails currentUser ) {




        return holidayService.updateLeaveStatus(jsonLeaveString, Integer.parseInt(currentUser.getUsername()));
    }

    /**
     *
     * @param jsonLeaveString contains holidayId: holiday to accept, staffId: staff member who's holiday it is, holidayStatus: the status to change to
     * @return "success" or "error" depending if method was successful
     */
    @RequestMapping(value = "/line_manager/manage_my_staff/AcceptLeave", method = RequestMethod.POST)
    public String lineManagerAcceptLeave(@RequestBody String jsonLeaveString) {


        return holidayService.updateLeaveStatus(jsonLeaveString,0);
    }


    /**
     *
     * @param jsonLeaveString contains holiday object values
     * @return "success" or "error" depending if method was successful
     */
    @RequestMapping(value = "/line_manager/manage_my_staff/createAltLeave", method = RequestMethod.POST)
    public String createAltLeave(@RequestBody String jsonLeaveString) {


        return holidayService.createAltLeave(jsonLeaveString);
    }

    /**
     * Simple decline for leave with only 3 params in json string
     * @param jsonLeaveString contains staffId, holidayId and manager comment
     * @return "success" or "error" depending if method was successful
     */
    @RequestMapping(value = "/line_manager/manage_my_staff/simpleManagerDeclineLeave", method = RequestMethod.POST)
    public String simpleDeclineLeave(@RequestBody String jsonLeaveString) {


        return holidayService.simpleManagerDeclineLeave(jsonLeaveString);
    }
}