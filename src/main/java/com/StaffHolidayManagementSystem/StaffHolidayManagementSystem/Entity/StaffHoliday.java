package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

/** StaffHoliday Entity class
 * @author John Kennedy
 * @version 1.0
 */

@Entity
@Table(name = "holiday")
public class StaffHoliday {

    @Id
    @Column(name = "holiday_id", nullable = false)
    private int holidayId;

    @Column(name = "start_date", nullable = false)
    private LocalDateTime startDateTime;

    @Column(name = "end_date", nullable = false)
    private LocalDateTime endDateTime;

    @Column(name = "holiday_reason")
    private String holidayReason;

    @Column(name = "absence_type", nullable = false)
    private AbsenceType absenceType;

    @Column(name = "manager_comment", nullable = false)
    private String managerComment;

    @Column(name = "holiday_status", nullable = false)
    private HolidayStatus holidayStatus;

    @Column(name = "alt_holiday_id", nullable = false)
    private int altHolidayId;

    public StaffHoliday(int holidayId, LocalDateTime startDateTime, LocalDateTime endDateTime, String holidayReason,
                        AbsenceType absenceType, String managerComment, HolidayStatus holidayStatus, int altHolidayId) {
        this.holidayId = holidayId;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.holidayReason = holidayReason;
        this.absenceType = absenceType;
        this.managerComment = managerComment;
        this.holidayStatus = holidayStatus;
        this.altHolidayId = altHolidayId;
    }

    public StaffHoliday(){

    }


    public int getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(int holidayId) {
        this.holidayId = holidayId;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getHolidayReason() {
        return holidayReason;
    }

    public void setHolidayReason(String holidayReason) {
        this.holidayReason = holidayReason;
    }

    public AbsenceType getAbsenceType() {
        return absenceType;
    }

    public void setAbsenceType(AbsenceType absenceType) {
        this.absenceType = absenceType;
    }

    public String getManagerComment() {
        return managerComment;
    }

    public void setManagerComment(String managerComment) {
        this.managerComment = managerComment;
    }

    public HolidayStatus getHolidayStatus() {
        return holidayStatus;
    }

    public void setHolidayStatus(HolidayStatus holidayStatus) {
        this.holidayStatus = holidayStatus;
    }

    public int getAltHolidayId() {
        return altHolidayId;
    }

    public void setAltHolidayId(int altHolidayId) {
        this.altHolidayId = altHolidayId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StaffHoliday that = (StaffHoliday) o;
        return holidayId == that.holidayId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(holidayId);
    }
}
