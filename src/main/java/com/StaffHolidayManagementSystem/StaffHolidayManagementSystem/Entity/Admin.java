package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity;


/** Admin Class
 * @author John Kennedy
 * @version 1.0
 */

public class Admin extends StaffMember {
    public Admin(int staffMemberId, String staffPassword, String forename, String surname, String department, int lineManagerStaffId, String eMail, float leaveEntitlement, float leaveRemaining, boolean enabled) {
        super(staffMemberId, staffPassword, forename, surname, department, lineManagerStaffId, eMail, leaveEntitlement, leaveRemaining, enabled);
    }

    public Admin() {
    }
}
