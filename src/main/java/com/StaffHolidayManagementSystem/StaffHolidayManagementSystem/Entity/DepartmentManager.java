package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity;


/** Department Manager Class
 * @author John Kennedy
 * @version 1.0
 */

public class DepartmentManager extends StaffMember {
    public DepartmentManager() {
    }

    public DepartmentManager(int staffMemberId, String staffPassword, String forename, String surname, String department, int lineManagerStaffId, String eMail, float leaveEntitlement, float leaveRemaining, boolean enabled) {
        super(staffMemberId, staffPassword, forename, surname, department, lineManagerStaffId, eMail, leaveEntitlement, leaveRemaining, enabled);

    }
}
