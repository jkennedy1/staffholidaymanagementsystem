package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity;

import com.fasterxml.jackson.annotation.JsonCreator;


/** Enum for Holiday Status
 * @author John Kennedy
 * @version 1.0
 */

public enum HolidayStatus {


    ACCEPTED, ALTERNATIVEOFFER, DECLINED, PENDING, ARCHIVED, DECLINED_ARCHIVED, DECLINED_ALT_OFFER_ARCHIVED, CANCELLED_ARCHIVED;


    @JsonCreator
    public static HolidayStatus fromValue(String value) {

        return HolidayStatus.valueOf(value);
    }


}
