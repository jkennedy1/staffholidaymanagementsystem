package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;


/** Enum of AbsenceTypoes
 * @author John Kennedy
 * @version 1.0
 */

@JsonFormat
public enum AbsenceType {

    ADOPTIONLEAVE, PATERNITYBIRTH, VACATIONLEAVE, MATERNITYLEAVE, PAIDLEAVE, OTHERLEAVE, SICKNESS, UNAVAILABLE, PATERNITYADOPTION, UNPAIDLEAVE, JURYDUTY;


    @JsonCreator
    public static AbsenceType fromValue(String value) {

        return AbsenceType.valueOf(value);
    }
}
