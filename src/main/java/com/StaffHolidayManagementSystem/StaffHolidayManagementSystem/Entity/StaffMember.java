package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO.StaffType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/** StaffMember Entity class
 * @author John Kennedy
 * @version 1.0
 */

@Entity
@Table(name = "staff_member")
public class StaffMember {

    @Id
    @Column(name = "staff_member_id", nullable = false)
    private int staffMemberId;

    @Column(name = "staff_member_role", nullable = false)
    private StaffType staffMemberRole;

    @Column(name = "staff_password", nullable = false)
    private String staffPassword;

    @Column(name = "forename", nullable = false)
    private String forename;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "department", nullable = false, length = 3)
    private String department;

    //If lineManagerStaffId = 0  StaffMember has no line_manager_staff_id
    @Column(name = "line_manager_staff_id")
    private int lineManagerStaffId;

    @Column(name = "e_mail", nullable = false, length = 320)
    private String eMail;

    @Column(name = "leave_entitlement", nullable = false)
    private float leaveEntitlement;

    @Column(name = "leave_remaining", nullable = false)
    private float leaveRemaining;

    //is the account active (for spring security)
    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Transient
    private List<StaffHoliday> staffHolidays = new ArrayList<>();


    public StaffMember(int staffMemberId, String staffPassword, String forename, String surname, String department, int lineManagerStaffId, String eMail, float leaveEntitlement, float leaveRemaining, boolean enabled) {
        this.staffMemberId = staffMemberId;
        this.staffMemberRole = StaffType.ROLE_STAFF;
        this.forename = forename;
        this.surname = surname;
        this.department = department.toUpperCase();
        this.lineManagerStaffId = lineManagerStaffId;
        this.eMail = eMail;
        this.staffPassword = staffPassword;
        this.leaveRemaining = leaveRemaining;
        this.leaveEntitlement = leaveEntitlement;
        this.enabled = enabled;
    }

    public StaffMember() {
    this.staffMemberRole = StaffType.ROLE_STAFF;
    }

    public int getStaffMemberId() {
        return staffMemberId;
    }

    public void setStaffMemberId(int staffMemberId) {
        this.staffMemberId = staffMemberId;
    }

    public StaffType getStaffMemberRole() {
        return staffMemberRole;
    }

    public void setStaffMemberRole(StaffType staffMemberRole) {
        this.staffMemberRole = staffMemberRole;
    }

    public String getStaffPassword() {
        return staffPassword;
    }

    public void setStaffPassword(String staffPassword) {
        this.staffPassword = staffPassword;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * If lineManagerStaffId = 0  StaffMember has no line manager
     */
    public int getLineManagerStaffId() {
        return lineManagerStaffId;
    }

    public void setLineManagerStaffId(int lineManagerStaffId) {
        this.lineManagerStaffId = lineManagerStaffId;
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }


    public List<StaffHoliday> getStaffHolidays() {
        return this.staffHolidays;
    }

    public void addStaffHoliday(StaffHoliday staffHoliday) {

        staffHolidays.add(staffHoliday);
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public float getLeaveEntitlement() {
        return leaveEntitlement;
    }

    public void setLeaveEntitlement(float leaveEntitlement) {
        this.leaveEntitlement = leaveEntitlement;
    }

    public float getLeaveRemaining() {
        return leaveRemaining;
    }

    public void setLeaveRemaining(float leaveRemaining) {
        this.leaveRemaining = leaveRemaining;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StaffMember that = (StaffMember) o;
        return staffMemberId == that.staffMemberId &&
                lineManagerStaffId == that.lineManagerStaffId &&
                Float.compare(that.leaveEntitlement, leaveEntitlement) == 0 &&
                Float.compare(that.leaveRemaining, leaveRemaining) == 0 &&
                enabled == that.enabled &&
                staffMemberRole == that.staffMemberRole &&
                staffPassword.equals(that.staffPassword) &&
                forename.equals(that.forename) &&
                surname.equals(that.surname) &&
                department.equals(that.department) &&
                eMail.equals(that.eMail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(staffMemberId, staffMemberRole, staffPassword, forename, surname, department, lineManagerStaffId, eMail, leaveEntitlement, leaveRemaining, enabled);
    }
}
