package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Service;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO.HolidayDao;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO.StaffDao;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO.StaffType;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffHoliday;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffMember;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper.PasswordEncrypter;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper.ReturnMessage;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/** Service class to handle Staff changes
 * @author John Kennedy
 * @version 1.0
 */
@Service
public class StaffService {

    private final StaffDao staffDao;

    private final HolidayDao holidayDao;

    private final PasswordEncrypter passwordEncrypter;



    @Autowired
    public StaffService(StaffDao staffDao, HolidayDao holidayDao) {
        this.staffDao = staffDao;
        this.holidayDao = holidayDao;
        this.passwordEncrypter = new PasswordEncrypter();

    }


    /**
     * Gets a StaffMember by id
     * @param id id of staff member
     * @return Staff Member
     */
    public StaffMember getStaffMemberById(int id) {

        StaffMember staffMember = staffDao.getStaffById(id);

        for (StaffHoliday holiday : holidayDao.getHolidaysByStaffId(id)) {

            staffMember.addStaffHoliday(holiday);
        }


        return staffMember;

    }

    /**
     * Gets all Staff By Department
     * @param departmentManagerId department managers id to get department from
     * @return List of StaffMembers
     */
    public List<StaffMember> getAllStaffByDepartment(int departmentManagerId) {

        StaffMember departmentManager = staffDao.getStaffById(departmentManagerId);

//Currently not using the department holidays
//        for (StaffMember staffMember : staffMemberList) {
//
//            for (StaffHoliday holiday : holidayDao.getHolidaysByStaffId(staffMember.getStaffMemberId())) {
//                staffMember.addStaffHoliday(holiday);
//            }
//
//        }
        return staffDao.getAllStaffByDepartment(departmentManager.getDepartment());

    }


    /**
     * Gets list of staff members who belong to calling line manager populated with holidays between startDate and endDate
     * @param startDate the date to look from (only month will be used)
     * @param endDate the date to look till (only month will be used)
     * @param lineManagerId id of the line manager
     * @return List of StaffMembers
     */
    public List<StaffMember> getAllStaffByLineManagerByMonth(int lineManagerId, LocalDateTime startDate, LocalDateTime endDate) {

        List<StaffMember> staffMemberList = staffDao.getAllStaffByLineManager(lineManagerId);


        for (StaffMember staffMember : staffMemberList) {

            for (StaffHoliday holiday : holidayDao.getLeaveEventsForCalendar(staffMember.getStaffMemberId(), startDate, endDate)) {
                staffMember.addStaffHoliday(holiday);
            }

        }
        return staffMemberList;

    }

    /**
     * Gets list of staff members who belong to calling line manager populated with holidays between startDate and endDate
     * @param year the year to get holidays from (by startDate)
     * @param lineManagerId the line manager id calling the method
     * @return List of StaffMembers
     */
    public List<StaffMember> getAllStaffByLineManagerByYear(int lineManagerId, int year) {

        List<StaffMember> staffMemberList = staffDao.getAllStaffByLineManager(lineManagerId);


        for (StaffMember staffMember : staffMemberList) {

            for (StaffHoliday holiday : holidayDao.getAcceptedAndPendingHolidaysByYear(staffMember.getStaffMemberId(), year)) {
                staffMember.addStaffHoliday(holiday);
            }

        }
        return staffMemberList;

    }

    /**
     * Gets list of staff members who belong to calling line manager populated with all their holidays
     * @param id the line manager id calling the method
     * @return List of StaffMembers
     */
    public List<StaffMember> getAllStaffByLineManager(int id) {
        List<StaffMember> staffMemberList = staffDao.getAllStaffByLineManager(id);

        for (StaffMember staffMember : staffMemberList) {

            for (StaffHoliday holiday : holidayDao.getHolidaysByStaffId(staffMember.getStaffMemberId())) {
                staffMember.addStaffHoliday(holiday);
            }

        }
        return staffMemberList;

    }

    /**
     * Gets all staff but the calling admin
     * @param adminId the calling admin's ID
     * @return List of StaffMembers
     */
    public List<StaffMember> getAllStaff(int adminId) {

        StaffMember admin = staffDao.getStaffById(adminId);

        if (admin.getStaffMemberRole().equals(StaffType.ROLE_ADMIN)) {
            return staffDao.getAllStaff(adminId);
        } else return new ArrayList<>();


    }

    /**
     * Toggle StaffMembers account status between true(activated) and false(de-activated)
     * @param adminId the calling admin's ID
     * @param staffId the staff whos account to change
     * @return "success" if staff updated or "error" if failed
     */
    public String toggleEnabledStaffById(int adminId, int staffId) {


        StaffMember admin = staffDao.getStaffById(adminId);

        if (admin.getStaffMemberRole().equals(StaffType.ROLE_ADMIN)) {

            return ReturnMessage.getByValue(staffDao.toggleEnabledStaffById(staffId)).toString();

        } else return ReturnMessage.ERROR.toString();
    }





    /**
     * Updates StaffMembers role,department,leaveEntitlement, leaveRemaining by their staff ID
     * @param jsonString Json String containing role,department,leaveEntitlement, leaveRemaining, staffId
     * @param adminId the calling admin's ID
     * @return "success" if staff updated or "error" if failed
     */
    public String updateStaffById(int adminId, String jsonString) {
        StaffMember admin = staffDao.getStaffById(adminId);

        if (admin.getStaffMemberRole().equals(StaffType.ROLE_ADMIN)) {

            JSONObject obj = new JSONObject(jsonString);


            return ReturnMessage.getByValue(staffDao.updateStaff(StaffType.valueOf(obj.getString("staffMemberRole")), obj.getString("department"),
                    obj.getFloat("leaveEntitlement"), obj.getFloat("leaveRemaining"), obj.getInt("staffId"))).toString();

        } else return ReturnMessage.ERROR.toString();
    }

    public List<StaffMember> getAllLineManagers() {

        return staffDao.getAllLineManagers();
    }


    /**
     * Add Staff Member to system from jsonString
     * @param jsonString jsonString of Staff Member (staffMemberRole,department,leaveEntitlement,leaveRemaining)
     * @return id of created Staff Member
     */
    public int addStaff(String jsonString) {


        JSONObject obj = new JSONObject(jsonString);

        String encryptedPassword = passwordEncrypter.encryptPassword(obj.getString("password"));

        StaffMember staffMember = new StaffMember(0, encryptedPassword, obj.getString("forename"), obj.getString("surname"), obj.getString("department"),
                obj.getInt("lineManagerId"), obj.getString("eMail"), obj.getFloat("leaveEntitlement"), obj.getFloat("leaveEntitlement"),
                true);

        if (staffDao.addStaff(staffMember) == 1) {

            return getStaffIdFromList(staffDao.getAllStaffByLineManager(staffMember.getLineManagerStaffId()), staffMember);

        }

        return staffDao.addStaff(staffMember);


    }

    //Try to find the  staffMember from the list by using a partially complete staffMember
    //return ID when found

    /**
     * Find the staffMember from the list by using a partially complete staffMember (Surname and Password)
     * @param staffMemberList list of StaffMembers
     * @param staffMemberToFind StaffMember needs Surname and Password to compare
     * @return staffMembers ID
     */
    private int getStaffIdFromList(List<StaffMember> staffMemberList, StaffMember staffMemberToFind) {

        for (StaffMember sm : staffMemberList) {
            if (sm.getSurname().equals(staffMemberToFind.getSurname()) && sm.getStaffPassword().equals(staffMemberToFind.getStaffPassword())) {
                return sm.getStaffMemberId();
            }
        }
        return 0;
    }
}

