package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Service;


import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO.HolidayDao;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO.StaffDao;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.HolidayStatus;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffHoliday;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffMember;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper.JsonToStaffHoliday;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper.LeaveCalculator;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper.ReturnMessage;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/** Service class to handle holidays changes
 * @author John Kennedy
 * @version 1.0
 */
@Service
public class HolidayService {

    private final
    HolidayDao holidayDao;

    private final LeaveCalculator leaveCalculator;

    private final
    StaffDao staffDao;


    @Autowired
    public HolidayService(HolidayDao holidayDao, StaffDao staffDao) {
        this.holidayDao = holidayDao;
        this.staffDao = staffDao;
        this.leaveCalculator = new LeaveCalculator();
    }


    /**
     * Get StaffHoliday object by holidayId and staffId
     * @param holidayId Holiday ID
     * @param staffMemberId StaffMembers ID
     * @return StaffHoliday object
     */
    public StaffHoliday getHolidayById(int holidayId, int staffMemberId) {
        return holidayDao.getHolidayById(holidayId, staffMemberId);
    }

    /**
     * Get all holidays by StaffMembers ID
     * @param id staffMembers ID
     * @return List of StaffHolidays
     */
    public List<StaffHoliday> getHolidaysByStaffId(int id) {
        return holidayDao.getHolidaysByStaffId(id);
    }


    /**
     * Creates and adds holiday to database from json string
     * @param jsonHoliday holiday json string
     * @param staffMemberId staff member to add holiday to
     * @return "sucess" or "error" or "holiday dates already booked"
     */
    public String addHolidayByJsonString(String jsonHoliday, int staffMemberId) {

        StaffHoliday sh = new JsonToStaffHoliday().convertJsonObjToStaffHoliday(new JSONObject(jsonHoliday));

        List<StaffHoliday> currHolidayList = holidayDao.getAcceptedAndPendingHolidaysByYear(staffMemberId, sh.getStartDateTime().getYear());


        if (isLeaveDateAlreadyBooked(currHolidayList, sh)) {
            return ReturnMessage.HOLIDAY_DATES_ALREADY_BOOKED.toString();
        }

        if (deductLeave(sh.getStartDateTime(), sh.getEndDateTime(), staffMemberId).equals("success")) {
            return ReturnMessage.getByValue(holidayDao.addHoliday(sh, staffMemberId)).toString();
        }
        return ReturnMessage.ERROR.toString();
    }

    /**
     * Checks if the date is already booked by the StaffMember
     * @param currHolidays list of current holidays with status 'ACCEPTED' or 'PENDING'
     * @param newHoliday the new holiday to test against
     * @return true if date is already booked, false if not
     */
    private boolean isLeaveDateAlreadyBooked(List<StaffHoliday> currHolidays, StaffHoliday newHoliday) {


        for (StaffHoliday staffHoliday : currHolidays) {
            if(staffHoliday.getStartDateTime().equals(newHoliday.getStartDateTime())){
                return true;
            }
            if(staffHoliday.getEndDateTime().equals(newHoliday.getEndDateTime())){
                return true;
            }
            if (newHoliday.getStartDateTime().isAfter(staffHoliday.getStartDateTime()) && newHoliday.getStartDateTime().isBefore(staffHoliday.getEndDateTime())) {
                System.out.println("StaffHoliday Start Date: " + staffHoliday.getStartDateTime());
                return true;
            } else if (newHoliday.getEndDateTime().isAfter(staffHoliday.getStartDateTime()) && newHoliday.getEndDateTime().isBefore(staffHoliday.getEndDateTime())) {
                System.out.println("StaffHoliday End Date: " + staffHoliday.getEndDateTime());
                return true;
            }
        }
        return false;

    }

    /**
     * Change StaffMembers Holiday to leaveStatus value
     * @param holiday  holiday to change
     * @param staffMemberId id of the staff member who the holiday belongs to
     * @param leaveStatus the status to change the holiday to
     * @return "success" or "error"
     */
    public String declineLeave(StaffHoliday holiday, int staffMemberId, HolidayStatus leaveStatus) {

        if (refundLeave(holiday.getStartDateTime(), holiday.getEndDateTime(), staffMemberId).equals("success")) {
            return ReturnMessage.getByValue(holidayDao.declineLeaveById(holiday.getHolidayId(), staffMemberId, leaveStatus)).toString();
        }
        return ReturnMessage.ERROR.toString();
    }

    /**
     * Get the last five none-archived holidays for StaffMember
     * @param staffId staffMember's id who's holidays to collect
     * @return List of StaffHolidays
     */
    public List<StaffHoliday> getLastFiveNoneArchiveHolidaysByStaffId(int staffId) {


        return holidayDao.getLastFiveNoneArchivedHolidaysByStaffId(staffId);
    }

    /**
     * Get StaffHolidays between two dates for StaffMember
     * @param id id of StaffMember
     * @param startDate starting point to look from
     * @param endDate point to look till
     * @return List of StaffHolidays
     */
    public List<StaffHoliday> getLeaveEventsForCalendar(int id, LocalDateTime startDate, LocalDateTime endDate) {


        return holidayDao.getLeaveEventsForCalendar(id, startDate, endDate);
    }





    /**
     * Archive declined holiday then deduct leave and change alt holiday = 'Accepted'
     * @param jsonString json string containing alt holiday id and current holiday id
     * @param staffMemberId id of StaffMember who holiday belongs tp
     * @return "sucess" or "error"
     */
    public String acceptAlternativeOffer(String jsonString, int staffMemberId) {

        System.out.println(jsonString);
        JSONObject obj = new JSONObject(jsonString);

        StaffHoliday currHoliday = holidayDao.getHolidayById(obj.getInt("currHolidayId"), staffMemberId);
        StaffHoliday altHoliday = holidayDao.getHolidayById(obj.getInt("altHolidayId"), staffMemberId);


        if (declineLeave(currHoliday, staffMemberId, HolidayStatus.DECLINED_ARCHIVED).equals("success")) {
            if (deductLeave(altHoliday.getStartDateTime(), altHoliday.getEndDateTime(), staffMemberId).equals("success")) {
                return ReturnMessage.getByValue(holidayDao.updateLeaveStatus(altHoliday.getHolidayId(), staffMemberId, HolidayStatus.ACCEPTED)).toString();
            }
        }


        return ReturnMessage.ERROR.toString();
    }

    /**
     * Deducts leave amount from staff specified
     * @param startDate start date of holiday
     * @param endDate end date of holiday
     * @param staffId StaffMember id to deduct from
     * @return "success" or "error"
     */
    private String deductLeave(LocalDateTime startDate, LocalDateTime endDate, int staffId) {

        float workingDaysBetween = leaveCalculator.calculateBusinessDaysFromHalfDays(leaveCalculator.calculateFullWorkingDaysBetweenDates(startDate, endDate), startDate, endDate);

        return ReturnMessage.getByValue(staffDao.deductLeave(workingDaysBetween, staffId)).toString();
    }

    /**
     * Refund leave amount to staff specified
     * @param startDate start date of holiday
     * @param endDate end date of holiday
     * @param staffId StaffMember id to refund from
     * @return "success" or "error"
     */
    private String refundLeave(LocalDateTime startDate, LocalDateTime endDate, int staffId) {

        float workingDaysBetween = leaveCalculator.calculateBusinessDaysFromHalfDays(leaveCalculator.calculateFullWorkingDaysBetweenDates(startDate, endDate), startDate, endDate);

        return ReturnMessage.getByValue(staffDao.refundLeave(workingDaysBetween, staffId)).toString();
    }

    /**
     * Get all holidays for staff member and remove ones with HolidayStatus "ALTERNATIVEOFFER"
     * @param staffId StaffMembers id to get holidays for
     * @return List of StaffMembers
     */
    public List<StaffHoliday> getStaffLeaveHistoryByStaffId(int staffId) {

        List<StaffHoliday> shList = holidayDao.getHolidaysByStaffId(staffId);

        shList.removeIf(sh -> sh.getHolidayStatus() == HolidayStatus.ALTERNATIVEOFFER);


        return shList;
    }


    /**
     * update leave status for holiday
     * Else expects staffID to be in the jsonObject (i.e Manager Accepting holiday)
     * @param jsonLeaveString contains current holiday id, and HolidayStatus
     * @param currentUserId is used when staffID is not given in ajax post (i.e staff declining alt holiday)
     * @return "success" or "error"
     */
    public String updateLeaveStatus(String jsonLeaveString, int currentUserId) {
        JSONObject obj = new JSONObject(jsonLeaveString);


        if (currentUserId != 0) {
            return ReturnMessage.getByValue(holidayDao.updateLeaveStatus(obj.getInt("holidayId"), currentUserId, HolidayStatus.valueOf(obj.getString("holidayStatus")))).toString();
        }


        return ReturnMessage.getByValue(holidayDao.updateLeaveStatus(obj.getInt("holidayId"), obj.getInt("staffId"), HolidayStatus.valueOf(obj.getString("holidayStatus")))).toString();
    }

    /**
     * Creates alternative holiday object from json string and adds to database
     * if holiday is already booked return "dates already booked"
     * if not enough leave remaining return "not enough leave remaining"
     * if holiday is successfully added, decline old holiday
     * @param jsonLeaveString jsonString containing Holiday Object Values
     * @return "success" or "error"
     */
    public String createAltLeave(String jsonLeaveString) {

        System.out.println(jsonLeaveString);
        JSONObject obj = new JSONObject(jsonLeaveString);
        //leave belongs to this staff member
        int staffId = obj.getInt("staffId");


        //Create altLeave holiday obj
        JSONObject holiday = new JSONObject();
        holiday.put("holidayId", 0);
        holiday.put("startDateTime", obj.getString("newStartDateTime"));
        holiday.put("endDateTime", obj.getString("newEndDateTime"));
        holiday.put("holidayReason", obj.getString("oldHolidayReason"));
        holiday.put("absenceType", obj.getString("oldAbsenceType"));
        holiday.put("managerComment", "AlternativeOffer");
        holiday.put("holidayStatus", obj.getString("newHolidayStatus"));
        holiday.put("altHolidayId", 0);

        StaffHoliday altHoliday = new JsonToStaffHoliday().convertJsonObjToStaffHoliday(holiday);
        StaffHoliday oldHoliday = holidayDao.getHolidayById(obj.getInt("oldHolId"), staffId);

        List<StaffHoliday> currHolidayList = holidayDao.getAcceptedAndPendingHolidaysByYear(staffId, altHoliday.getStartDateTime().getYear());

        if (!isLeaveDateAlreadyBooked(currHolidayList, altHoliday)) {
            return ReturnMessage.HOLIDAY_DATES_ALREADY_BOOKED.toString();
        }

        //check if staff has enough remaining time for the leave
        if (altLeaveTimeRemainingCheck(oldHoliday, altHoliday, staffDao.getStaffById(staffId))) {

            //not enough leave
            return ReturnMessage.NOT_ENOUGH_LEAVE_REMAINING.toString();
        }


        //if altHoliday was added, update Declined Holiday
        if (ReturnMessage.getByValue(holidayDao.addHoliday(altHoliday, staffId)).equals(ReturnMessage.SUCCESS)) {

            return updateDeclinedLeave(altHoliday, staffId, obj);
        }

        return ReturnMessage.ERROR.toString();

    }

    //check if staff has enough time remaining for (alt offer - declined offer)

    /**
     * Check if StaffMember has enough time remaining for (alternative offer - decline offer)
     * @param oldHoliday declined holiday
     * @param altHoliday alt holiday
     * @param staffMember StaffMember who's leave is being checked
     * @return false if enough time is available, true if not
     */
    private boolean altLeaveTimeRemainingCheck(StaffHoliday oldHoliday, StaffHoliday altHoliday, StaffMember staffMember) {

        return (staffMember.getLeaveRemaining() < (leaveCalculator.calculateFullWorkingDaysBetweenDates(altHoliday.getStartDateTime(), altHoliday.getEndDateTime()) -
                leaveCalculator.calculateFullWorkingDaysBetweenDates(oldHoliday.getStartDateTime(), oldHoliday.getEndDateTime())));

    }





    /**
     * If alt holiday is found, then update declined holiday with managerComment, altHolidayId and holidayStatus
     * then refund declined holiday leave amount back to StaffMember
     * @param altHoliday alt holiday to look for
     * @param staffId id of staff who owns holiday
     * @param obj containing declined leave updates
     * @return deducted leave back to staff
     */
    private String updateDeclinedLeave(StaffHoliday altHoliday, int staffId, JSONObject obj) {

        //get alt holiday id
        int altHolidayId = holidayDao.getHolidayIdByStartAndEndDate(staffId, altHoliday.getStartDateTime(), altHoliday.getEndDateTime());


        if (altHolidayId != 0) {
            //if alt holiday is found
            String managerComment = obj.getString("newManagerComment");
            int declinedHolidayId = obj.getInt("oldHolId");

            //update holiday to declined and with new values
            if (ReturnMessage.getByValue(holidayDao.updateDeclinedLeaveWithAltLeave(staffId, managerComment, declinedHolidayId, altHolidayId, HolidayStatus.DECLINED)).equals(ReturnMessage.SUCCESS)) {
                //get Declined Holiday
                StaffHoliday declinedHol = holidayDao.getHolidayById(declinedHolidayId, staffId);
                //refundLeave from the declined holiday
                return refundLeave(declinedHol.getStartDateTime(), declinedHol.getEndDateTime(), staffId);
            }

        }


        return ReturnMessage.ERROR.toString();

    }


    //refund leave then decline holiday

    /**
     * Refunds leave amount of holiday to be declined then set status to declined and add  manager comment
     * @param jsonLeaveString  jsonString of values to update holiday
     * @return "sucess" or "error"
     */
    public String simpleManagerDeclineLeave(String jsonLeaveString) {


        JSONObject obj = new JSONObject(jsonLeaveString);
        int staffMemberId = obj.getInt("staffId");

        StaffHoliday staffHoliday = holidayDao.getHolidayById(obj.getInt("holidayId"), staffMemberId);

        if (refundLeave(staffHoliday.getStartDateTime(), staffHoliday.getEndDateTime(), staffMemberId).equals(ReturnMessage.SUCCESS.toString())) {
            return ReturnMessage.getByValue(holidayDao.simpleManagerDeclineLeave(obj.getInt("staffId"), obj.getInt("holidayId"), obj.getString("managerComment"), HolidayStatus.DECLINED)).toString();

        }

        return ReturnMessage.ERROR.toString();


    }
}



