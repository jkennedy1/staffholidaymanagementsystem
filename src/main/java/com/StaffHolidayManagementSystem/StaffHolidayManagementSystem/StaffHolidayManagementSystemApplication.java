package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class StaffHolidayManagementSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(StaffHolidayManagementSystemApplication.class, args);
    }


}

