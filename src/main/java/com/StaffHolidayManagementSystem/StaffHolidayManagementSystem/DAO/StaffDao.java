package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffMember;

import java.util.List;

/** Database interface for Staff Methods
 * @author John Kennedy
 * @version 1.0
 */

public interface StaffDao {

    /**
     * Gets all StaffMembers by department
     * @param department department code
     * @return List of staff members
     */
    List<StaffMember> getAllStaffByDepartment(String department);

    /**
     * Get all staff members for given line manager
     * @param lineManagerId id of line manager to get staff for
     * @return List of staff members
     */
    List<StaffMember> getAllStaffByLineManager(int lineManagerId);

    /**
     * Get StaffMember by staff ID
     * @param id id of the staff member to get
     * @return StaffMember object
     */
    StaffMember getStaffById(int id);

    /**
     * Add StaffMember to Database
     * @param staffMember StaffMember object to add
     * @return number of rows effected
     */
    int addStaff(StaffMember staffMember);

    /**
     * Updates staffmembers values
     * @param staffRole StaffType to update to
     * @param department  department to update to
     * @param leaveEntitlement leaveEntitlement to update to
     * @param leaveRemaining leaveRemaining to update to
     * @param staffId id of StaffMember to update
     * @return number of rows effected
     */
    int updateStaff(StaffType staffRole, String department, float leaveEntitlement, float leaveRemaining, int staffId);

    /**
     * Toggles staff account status
     * @param id id of staff member to update
     * @return number of rows effected
     */
    int toggleEnabledStaffById(int id);

    /**
     * Gets all Staff
     * @param adminId id of admin who called method
     * @return List of StaffMembers
     */
    List<StaffMember> getAllStaff(int adminId);

    /**
     * Get all Line Managers
     * @return List of StaffMembers
     */
    List<StaffMember> getAllLineManagers();

    /**
     * Refunds leave amount to staff specified
     * @param amount amount to refund
     * @param staffId StaffMember to refund to
     * @return number of rows effected
     */
    int refundLeave(float amount, int staffId);

    /**
     * Deducts leave amount from staff specified
     * @param amount amount to deduct
     * @param staffId StaffMember to deduct from
     * @return number of rows effected
     */
    int deductLeave(float amount, int staffId);
}
