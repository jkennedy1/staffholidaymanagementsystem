package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.HolidayStatus;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffHoliday;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
@Primary
public class HolidayDaoImpl implements HolidayDao {

    private final JdbcTemplate jdbc;
    private String sql;


    @Autowired
    public HolidayDaoImpl(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public StaffHoliday getHolidayById(int id, int staffMemberId) {

        sql = "SELECT * FROM holiday WHERE holiday_id = " + id + " And staff_member_id = " + staffMemberId;


        return jdbc.query(sql, new StaffHolidayMapper()).get(0);
    }

    @Override
    public List<StaffHoliday> getHolidaysByStaffId(int staffId) {

        sql = "SELECT * FROM holiday WHERE staff_member_id = " + staffId;

        return jdbc.query(sql, new StaffHolidayMapper());
    }


    @Override
    public List<StaffHoliday> getAcceptedAndPendingHolidaysByYear(int id, int year) {

        sql = "SELECT * FROM holiday WHERE (staff_member_id =" + id + ") AND (SELECT YEAR(start_date) ='" + year + "') " + "AND (holiday_status = 'PENDING' OR holiday_status = 'ACCEPTED')";

        return jdbc.query(sql, new StaffHolidayMapper());


    }

    @Override
    public int addHoliday(StaffHoliday staffHoliday, int staffMemberId) {

        sql = "INSERT INTO holiday (staff_member_id, start_date, end_date, holiday_reason, absence_type, manager_comment, holiday_status, alt_holiday_id) VALUES (?,?,?,?,?,?,?,?)";



        return jdbc.update(sql, staffMemberId, staffHoliday.getStartDateTime(), staffHoliday.getEndDateTime(), staffHoliday.getHolidayReason(), staffHoliday.getAbsenceType().name(), staffHoliday.getManagerComment(), staffHoliday.getHolidayStatus().name(), staffHoliday.getAltHolidayId());
    }

    @Override
    public int declineLeaveById(int holidayId, int staffMemberId, HolidayStatus leaveStatus) {

        sql = "UPDATE holiday SET holiday_status = '"+leaveStatus.name()+ "'  WHERE holiday_id =" + holidayId + " AND staff_member_id = " + staffMemberId;


        return jdbc.update(sql);
    }


    @Override
    public List<StaffHoliday> getLastFiveNoneArchivedHolidaysByStaffId(int staffId) {


        sql = "SELECT * FROM holiday WHERE (staff_member_id = " + staffId + ") AND (holiday_status = '"+HolidayStatus.ACCEPTED.name()+"' OR holiday_status = '"+HolidayStatus.DECLINED.name()+"' OR holiday_status = '"+HolidayStatus.PENDING.name()+"') ORDER BY holiday_id DESC LIMIT 5";

        return jdbc.query(sql, new StaffHolidayMapper());
    }

    @Override
    public List<StaffHoliday> getLeaveEventsForCalendar(int id, LocalDateTime startDate, LocalDateTime endDate) {


        sql = "SELECT * FROM holiday WHERE (staff_member_id =" + id + ") AND (start_date BETWEEN '" + startDate + "' AND '" + endDate + "')" + "AND (holiday_status = 'PENDING' OR holiday_status = 'ACCEPTED')";

        return jdbc.query(sql, new StaffHolidayMapper());

    }


    @Override
    public int updateLeaveStatus(int holidayId, int staffId, HolidayStatus holidayStatus) {

        sql = "UPDATE holiday SET holiday_status = '" + holidayStatus.name() + "' WHERE holiday_id = " + holidayId + " AND staff_member_id = " + staffId;


        return jdbc.update(sql);

    }

    @Override
    public int getHolidayIdByStartAndEndDate(int staffId, LocalDateTime startDateTime, LocalDateTime endDateTime) {

        sql = "SELECT holiday_id FROM holiday WHERE (staff_member_id =" + staffId + ") AND (start_date = '" + startDateTime + "') AND (end_date = '" + endDateTime + "')";

        try {
            jdbc.queryForObject(sql, Integer.class);

            return jdbc.queryForObject(sql, Integer.class);

        } catch (Exception e) {

            return 0;
        }


    }

    @Override
    public int updateDeclinedLeaveWithAltLeave(int staffId, String managerComment, int declinedHolidayId, int altHolidayId, HolidayStatus holidayStatus) {

        sql = "UPDATE holiday SET manager_comment = '"+managerComment+"', alt_holiday_id = "+altHolidayId+", holiday_status = '"+ holidayStatus.name()+"' WHERE staff_member_id = "+ staffId+" AND holiday_id = "+declinedHolidayId;


        return jdbc.update(sql);

    }

    @Override
    public int simpleManagerDeclineLeave(int staffId, int holidayId, String managerComment, HolidayStatus holidayStatus) {

        sql = "UPDATE holiday SET holiday_status = '" + holidayStatus.name() + "', manager_comment = '"+managerComment+"' WHERE holiday_id = " + holidayId + " AND staff_member_id = " + staffId;



        return jdbc.update(sql);
    }






}
