package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.AbsenceType;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.HolidayStatus;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffHoliday;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/** Class to map database results to StaffHolidays
 * @author John Kennedy
 * @version 1.0
 */

public class StaffHolidayMapper implements RowMapper<StaffHoliday> {


    public StaffHoliday mapRow(ResultSet rs, int rowNum) throws SQLException {

        StaffHoliday staffHoliday = new StaffHoliday();


        staffHoliday.setHolidayId(rs.getInt("holiday_id"));
        staffHoliday.setStartDateTime(rs.getTimestamp("start_date").toLocalDateTime());
        staffHoliday.setEndDateTime(rs.getTimestamp("end_date").toLocalDateTime());
        staffHoliday.setHolidayReason(rs.getString("holiday_reason"));
        staffHoliday.setAbsenceType(AbsenceType.valueOf(rs.getString("absence_type")));
        staffHoliday.setManagerComment(rs.getString("manager_comment"));
        staffHoliday.setHolidayStatus(HolidayStatus.valueOf(rs.getString("holiday_status")));
        staffHoliday.setAltHolidayId(rs.getInt("alt_holiday_id"));



        return staffHoliday;
    }

}
