package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Primary
public class StaffDaoImpl implements StaffDao {

    private final JdbcTemplate jdbc;
    private String sql;

    @Autowired
    public StaffDaoImpl(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public List<StaffMember> getAllStaffByDepartment(String department) {

        sql = "SELECT * FROM staff_member WHERE department = '" + department + "'";



        return jdbc.query(sql, new StaffMapper());

    }

    @Override
    public List<StaffMember> getAllStaffByLineManager(int lineManagerStaffId) {
        sql = "SELECT * FROM staff_member WHERE line_manager_staff_id = " + lineManagerStaffId + "";


        return jdbc.query(sql, new StaffMapper());
    }

    @Override
    public StaffMember getStaffById(int id) {
        sql = "SELECT * FROM staff_member WHERE staff_member_id = " + id + "";

        return jdbc.query(sql, new StaffMapper()).get(0);
    }

    @Override
    public int addStaff(StaffMember staffMember) {

        sql = "INSERT INTO staff_member(staff_member_role, staff_password, forename, surname, department, line_manager_staff_id, e_mail,leave_entitlement, leave_remaining, enabled )" +
                "VALUES (?,?,?,?,?,?,?,?,?,?)";



        return jdbc.update(sql, staffMember.getStaffMemberRole().name(), staffMember.getStaffPassword(), staffMember.getForename(),
                staffMember.getSurname(), staffMember.getDepartment(), staffMember.getLineManagerStaffId(), staffMember.getEMail(), staffMember.getLeaveEntitlement(), staffMember.getLeaveRemaining(), staffMember.isEnabled());

    }

    @Override
    public int updateStaff(StaffType staffRole, String department, float leaveEntitlement, float leaveRemaining, int staffId) {

        sql = "UPDATE staff_member SET staff_member_role = '" + staffRole.name() + "' , department = '" + department + "', " +
                "leave_entitlement = " + leaveEntitlement + " , leave_remaining = " + leaveRemaining + " WHERE staff_member_id = " + staffId;


        return jdbc.update(sql);

    }

    //flip the enabled boolean
    @Override
    public int toggleEnabledStaffById(int id) {
        sql = "UPDATE staff_member SET enabled = !enabled WHERE staff_member_id = " + id;
        return jdbc.update(sql);
    }

    //all but the admin requesting, for safety reasons
    @Override
    public List<StaffMember> getAllStaff(int adminId) {
        sql = "SELECT * FROM staff_member WHERE NOT staff_member_id = " + adminId;

        return jdbc.query(sql, new StaffMapper());
    }

    @Override
    public List<StaffMember> getAllLineManagers() {

        sql = "SELECT * FROM staff_member WHERE staff_member_role = '"+StaffType.ROLE_LINE_MANAGER+"'";

        return jdbc.query(sql, new StaffMapper());
    }

    @Override
    public int refundLeave(float amount, int staffId) {


        sql = "UPDATE staff_member SET leave_remaining = (leave_remaining + " + amount + ") WHERE staff_member_id =" + staffId;



        return jdbc.update(sql);
    }

    @Override
    public int deductLeave(float amount, int staffId) {




        sql = "UPDATE staff_member SET leave_remaining = (leave_remaining - " + amount + ") WHERE staff_member_id =" + staffId;




        return jdbc.update(sql);
    }



}
