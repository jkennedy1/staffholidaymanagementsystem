package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.Admin;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.DepartmentManager;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.LineManager;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffMember;

import java.util.Arrays;


/** Enum of StaffRole/Type
 * @author John Kennedy
 * @version 1.0
 */

public enum StaffType {

    ROLE_STAFF("ROLE_STAFF") {
            public StaffMember initialiseStaffMember() {
                return new StaffMember();
            }
    },
    ROLE_LINE_MANAGER("ROLE_LINE_MANAGER") {
        public StaffMember initialiseStaffMember() {
            StaffMember staffMember = new LineManager();
            staffMember.setStaffMemberRole(StaffType.ROLE_LINE_MANAGER);

            return  staffMember;
        }
    },
   ROLE_DEPARTMENT_MANAGER("ROLE_DEPARTMENT_MANAGER") {
        public StaffMember initialiseStaffMember() {
            StaffMember staffMember = new DepartmentManager();
            staffMember.setStaffMemberRole(StaffType.ROLE_DEPARTMENT_MANAGER);

            return  staffMember;
        }
    },
    ROLE_ADMIN("ROLE_ADMIN") {
        public StaffMember initialiseStaffMember() {
            StaffMember staffMember = new Admin();
            staffMember.setStaffMemberRole(StaffType.ROLE_ADMIN);

            return  staffMember;
        }
    };

    private String key;

    StaffType(String key) {
        this.key = key;
    }

    /**
     * Gets Enum value of string given
     * @param key String to get enum value from
     * @return Enum of StaffType
     */
    public static StaffType getForKey(String key){
        return Arrays.stream(values()).filter(type -> type.key.equals(key)).findFirst().orElse(ROLE_STAFF);
    }


    /**
     * initialises StaffMember depending on their role
     * @return StaffMember object of specified child role
     */
    public abstract StaffMember initialiseStaffMember();


}
