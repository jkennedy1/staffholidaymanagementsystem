package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.HolidayStatus;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffHoliday;

import java.time.LocalDateTime;
import java.util.List;

/** Database interface for holidays
 * @author John Kennedy
 * @version 1.0
 */

public interface HolidayDao {

    /**
     * Get StaffHoliday object by holidayId and staffId
     * @param id Holiday ID
     * @param staffMemberId StaffMembers ID
     * @return StaffHoliday object
     */
    StaffHoliday getHolidayById(int id, int staffMemberId);

    /**
     * Get all holidays by StaffMembers ID
     * @param staffId staffMembers ID
     * @return List of StaffHolidays
     */
    List<StaffHoliday> getHolidaysByStaffId(int staffId);

    /**
     * Get only holidays with HolidayStatus = 'ACCEPTED' or 'PENDING' in given year for given StaffMember
     * @param id id of StaffMember
     * @param year year of holidays
     * @return List of StaffHolidays
     */
    List<StaffHoliday> getAcceptedAndPendingHolidaysByYear(int id, int year);

    /**
     * add holiday to database
     * @param staffHoliday Holiday to add
     * @param staffMemberId Staff member to add it to
     * @return number of rows effected
     */
    int addHoliday(StaffHoliday staffHoliday, int staffMemberId);

    /**
     * Change StaffMembers Holiday to leaveStatus value
     * @param holidayId id of the holiday to change
     * @param staffMemberId id of the staff member who the holiday belongs to
     * @param leaveStatus the status to change the holiday to
     * @return number of rows effected
     */
    int declineLeaveById(int holidayId, int staffMemberId, HolidayStatus leaveStatus);

    /**
     * Get the last five none-archived holidays for StaffMember
     * @param staffId staffMember's id who's holidays to collect
     * @return List of StaffHolidays
     */
    List<StaffHoliday> getLastFiveNoneArchivedHolidaysByStaffId(int staffId);

    /**
     * Get StaffHolidays between two dates for StaffMember
     * @param id id of StaffMember
     * @param startDate starting point to look from
     * @param endDate point to look till
     * @return List of StaffHolidays
     */
    List<StaffHoliday> getLeaveEventsForCalendar(int id, LocalDateTime startDate, LocalDateTime endDate);


    /**
     * Update specified holidays status
     * @param holidayId holiday to update
     * @param staffId staffMember who's holiday to update
     * @param holidayStatus HolidayStatus to update to
     * @return number of rows effected
     */
    int updateLeaveStatus(int holidayId, int staffId, HolidayStatus holidayStatus);

    /**
     *  Gets Holiday Id with equals Start and End date for StaffMember
     * @param staffId id of staff who the holiday belongs to
     * @param startDateTime startDate of holiday
     * @param endDateTime endDate of holiday
     * @return holiday Id if holiday is found or 0 if none found
     */
    int getHolidayIdByStartAndEndDate(int staffId, LocalDateTime startDateTime, LocalDateTime endDateTime);

    /**
     * Updates declined holiday with manager comments, alt holiday id and HolidayStatus
     * @param staffId id of staff who the holiday belongs to
     * @param managerComment manager comment to update
     * @param declinedHolidayId the declined holidays id
     * @param altHolidayId id of the alt holiday to add to the declined holiday
     * @param holidayStatus the HolidayStatus
     * @return number of rows effected
     */
    int updateDeclinedLeaveWithAltLeave(int staffId, String managerComment, int declinedHolidayId, int altHolidayId, HolidayStatus holidayStatus);

    /**
     * Updates declined holiday with manager comments and HolidayStatus
     * @param staffId id of staff who the holiday belongs to
     * @param holidayId id of holiday to update
     * @param managerComment manager comment to update
     * @param holidayStatus HolidayStatus to update to
     * @return number of rows effected
     */
    int simpleManagerDeclineLeave(int staffId, int holidayId, String managerComment, HolidayStatus holidayStatus);


}
