package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.DAO;


import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffMember;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;


/** Class to map database results to StaffMembers
 * @author John Kennedy
 * @version 1.0
 */

public class StaffMapper implements RowMapper<StaffMember> {


    public StaffMember mapRow(ResultSet rs, int rowNum) throws SQLException {
        StaffMember staff = StaffType.getForKey(rs.getString("staff_member_role")).initialiseStaffMember();
        setupStaffMember(staff, rs);
        return staff;
    }

    private void setupStaffMember(StaffMember staffMember, ResultSet rs) throws SQLException {
        staffMember.setStaffMemberId(rs.getInt("staff_member_id"));
        staffMember.setDepartment(rs.getString("department"));
        staffMember.setForename(rs.getString("forename"));
        staffMember.setSurname(rs.getString("surname"));
        //convertJsonObjToStaffHoliday null to 0
        rs.getInt("line_manager_staff_id");
        if(rs.wasNull()){staffMember.setLineManagerStaffId(0);} else
        {staffMember.setLineManagerStaffId(rs.getInt("line_manager_staff_id"));}

        staffMember.setEMail(rs.getString("e_mail"));
        staffMember.setStaffPassword(rs.getString("staff_password"));
        staffMember.setLeaveEntitlement(rs.getFloat("leave_entitlement"));
        staffMember.setLeaveRemaining(rs.getFloat("leave_remaining"));
        staffMember.setEnabled(rs.getBoolean("enabled"));
    }


}

