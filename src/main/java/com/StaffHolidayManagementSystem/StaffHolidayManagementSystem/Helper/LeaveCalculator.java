package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.Set;

/** Class to calculate leave
 * @author John Kennedy
 * @version 1.0
 */
public class LeaveCalculator {



    /**
     * Calculate days between 2 dates without weekends
     * @param start startDate
     * @param end endDate
     * @return full working days between two dates. Excludes day of return
     */
    public long calculateFullWorkingDaysBetweenDates(final LocalDateTime start, final LocalDateTime end) {


        Set<DayOfWeek> weekend = EnumSet.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);

        return start.toLocalDate().datesUntil(end.toLocalDate())
                .filter(d -> !weekend.contains(d.getDayOfWeek()))
                .count();

    }

    //takes amount of working days, adds or subtracts hours depending on start/finish time
    //working day is 9-5 (8 hours)

    /**
     * Takes amount of working days, adds or subtracts hours depending on start/finish time
     * working day is 9-5 (8 hours)
     * @param workingDays amount of working days
     * @param startDate starting date
     * @param endDate end date
     * @return number of business days adapted for half days too
     */
    public float calculateBusinessDaysFromHalfDays(long workingDays, LocalDateTime startDate, LocalDateTime endDate) {

        float businessHours = workingDays * 8;

        //member has worked for 3 hours on start date so minus 3
        if (startDate.getHour() == 12) {
            businessHours -= 3.00F;
        }
        //member has 3 hours off on return date so add 3
        if (endDate.getHour() == 12) {
            businessHours += 3.00F;
        }



        //round up number to 2 decimal places
        return BigDecimal.valueOf(businessHours / 8).setScale(2, RoundingMode.HALF_UP).floatValue();
    }

}
