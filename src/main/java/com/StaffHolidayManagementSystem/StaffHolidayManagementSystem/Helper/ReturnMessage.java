package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper;

import java.util.HashMap;
import java.util.Map;

/** Enum for returning messages to controller
 * @author John Kennedy
 * @version 1.0
 */
public enum ReturnMessage {

    SUCCESS(1), ERROR(2), NOT_ENOUGH_LEAVE_REMAINING(3), HOLIDAY_DATES_ALREADY_BOOKED(4);

    private static final Map<Integer, ReturnMessage> MY_MAP = new HashMap<>();
    static {
        for (ReturnMessage returnMessage : values()) {
            MY_MAP.put(returnMessage.getValue(), returnMessage);
        }
    }
    private int value;

    ReturnMessage(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ReturnMessage getByValue(int value) {
        return MY_MAP.get(value);
    }

    public String toString() {
        switch (this.value){

            case 1: return "success";
            case 2: return "error";
            case 3: return "Not Enough Leave Remaining";
            case 4: return "Holiday Dates Booked";
            default: return null;
        }
    }

}
