package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper;

import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.AbsenceType;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.HolidayStatus;
import com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Entity.StaffHoliday;
import org.json.JSONObject;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/** Class to convert Json object into StaffHoliday object
 * @author John Kennedy
 * @version 1.0
 */
public class JsonToStaffHoliday {


    /**
     * Convert data UTC date String time to london/europe time
     * @param date to convert
     * @return LocalDateTime of converted date
     */
    public LocalDateTime utcToLondonDateTimeParser(String date) {



        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        LocalDateTime oldDateTime = LocalDateTime.parse(date, formatter);

        ZoneId oldZone = ZoneId.of("UTC");
        ZoneId newZone = ZoneId.of("Europe/London");

        return  oldDateTime.atZone(oldZone).withZoneSameInstant(newZone).toLocalDateTime();
    }

    /**
     * Convert JsonObject to StaffHoliday
     * @param jsonHoliday JsonObject of StaffHoliday
     * @return converted StaffHoliday
     */
    public StaffHoliday convertJsonObjToStaffHoliday(JSONObject jsonHoliday) {

        return new StaffHoliday(jsonHoliday.getInt("holidayId"), utcToLondonDateTimeParser(jsonHoliday.getString("startDateTime")), utcToLondonDateTimeParser(jsonHoliday.getString("endDateTime")), jsonHoliday.getString("holidayReason"),
                AbsenceType.valueOf(jsonHoliday.getString("absenceType")), jsonHoliday.getString("managerComment"), HolidayStatus.valueOf(jsonHoliday.getString("holidayStatus")), jsonHoliday.getInt("altHolidayId"));
    }
}
