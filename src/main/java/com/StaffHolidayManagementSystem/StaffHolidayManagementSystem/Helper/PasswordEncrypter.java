package com.StaffHolidayManagementSystem.StaffHolidayManagementSystem.Helper;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/** Class to bcrypt Password
 * @author John Kennedy
 * @version 1.0
 */
public class PasswordEncrypter {

    /**
     * Encrypts password string with BCrypt
     * @param passwordString password to encrypt
     * @return Encrypted password
     */
    public String encryptPassword(String passwordString){

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(passwordString);

        return hashedPassword;

    }

}
